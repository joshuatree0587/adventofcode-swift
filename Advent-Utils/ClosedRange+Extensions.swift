//
//  ClosedRange+Extensions.swift
//  Advent-Utils
//
//  Created by Joshua Stachowski on 12/4/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

extension ClosedRange {
    static func makeAscending<Bound>(_ a: Bound, _ b: Bound) -> ClosedRange<Bound> {
        return Swift.min(a, b)...Swift.max(a, b)
    }
    
    func encloses(_ other: ClosedRange) -> Bool {
        return self.lowerBound <= other.lowerBound && self.upperBound >= other.upperBound
    }
    
    func mergedWith(_ other: ClosedRange) -> ClosedRange {
        assert(self.overlaps(other), "Non-overlapping ranges cannot be merged")
        let lower = Swift.min(self.lowerBound, other.lowerBound)
        let upper = Swift.max(self.upperBound, other.upperBound)
        return lower...upper
    }
}

extension ClosedRange: Comparable {
    public static func < (lhs: ClosedRange<Bound>, rhs: ClosedRange<Bound>) -> Bool {
        if lhs.lowerBound == rhs.lowerBound {
            return lhs.upperBound < rhs.upperBound
        } else {
            return lhs.lowerBound < rhs.lowerBound
        }
    }
}
