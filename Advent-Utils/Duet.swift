//
//  Duet.swift
//  Advent-Utils
//
//  Created by Joshua Stachowski on 1/6/18.
//  Copyright © 2018 Joshua Stachowski. All rights reserved.
//

import Foundation

enum DuetState {
    case success
    case wait
    case exit
}

class DuetProgram {
    var partner: DuetProgram?

    var instructions: [DuetInstruction]
    var registers: [String: Int] = [:]
    var instructionCounts: [String: Int] = [:]
    var inputs: [Int] = []
    var sendCount = 0

    var cursor = 0

    var state = DuetState.success

    init(_ instructions: [DuetInstruction], _ id: Int) {
        self.instructions = instructions
        registers["p"] = id

        instructionCounts["snd"] = 0
        instructionCounts["set"] = 0
        instructionCounts["add"] = 0
        instructionCounts["sub"] = 0
        instructionCounts["mul"] = 0
        instructionCounts["mod"] = 0
        instructionCounts["rcv"] = 0
        instructionCounts["jnz"] = 0
        instructionCounts["jgz"] = 0
    }

    func update() {
        state = step()
    }

    private func step() -> DuetState {
        let instruction = instructions[cursor]
        instructionCounts[instruction.action] = instructionCounts[instruction.action]! + 1
        switch instruction.action {
        case "snd":
            let num = getValue(key: instruction.left)
            if partner == nil { inputs = [num] } else { partner!.inputs.append(num) }
            sendCount += 1
            cursor += 1
        case "set":
            putValue(value: instruction.right, key: instruction.left)
            cursor += 1
        case "add":
            let num = getValue(key: instruction.left) + getValue(key: instruction.right)
            putValue(value: num, key: instruction.left)
            cursor += 1
        case "sub":
            let num = getValue(key: instruction.left) - getValue(key: instruction.right)
            putValue(value: num, key: instruction.left)
            cursor += 1
        case "mul":
            let num = getValue(key: instruction.left) * getValue(key: instruction.right)
            putValue(value: num, key: instruction.left)
            cursor += 1
        case "mod":
            let num = getValue(key: instruction.left) % getValue(key: instruction.right)
            putValue(value: num, key: instruction.left)
            cursor += 1
        case "rcv":
            if partner == nil {
                print("Part One = \(inputs[0])")
                return .exit
            } else if inputs.count > 0 {
                putValue(value: inputs.removeFirst(), key: instruction.left)
                cursor += 1
            } else {
                return .wait
            }
        case "jnz":
            let num = getValue(key: instruction.left)
            cursor += num != 0 ? getValue(key: instruction.right) : 1
        case "jgz":
            let num = getValue(key: instruction.left)
            cursor += num > 0 ? getValue(key: instruction.right) : 1
        default:
            fatalError()
        }

        return cursor >= 0 && cursor < instructions.count ? .success : .exit
    }

    func getValue(key: String) -> Int {
        if let number = Int(key) {
            return number
        } else if let value = registers[key] {
            return value
        } else {
            registers[key] = 0
            return 0
        }
    }

    func putValue(value: Int, key: String) {
        registers[key] = value
    }

    func putValue(value: String, key: String) {
        if let number = Int(value) {
            registers[key] = number
        } else {
            registers[key] = getValue(key: value)
        }
    }
}

class DuetInstruction: CustomStringConvertible {
    var action: String
    var left: String
    var right: String

    public var description: String { return "\(action) \(left) \(right)" }

    init(data: String) {
        let terms = data.components(separatedBy: " ")
        action = terms[0]
        left = terms[1]
        right = terms.count > 2 ? terms[2] : ""
    }
}
