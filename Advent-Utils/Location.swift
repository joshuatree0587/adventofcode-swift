//
//  Location.swift
//  Advent-Utils
//
//  Created by Joshua Stachowski on 12/4/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

struct Location: Equatable, Hashable {
    static let origin = Location(0, 0)

    var x: Int
    var y: Int

    func hash(into hasher: inout Hasher) {
        hasher.combine(x)
        hasher.combine(y)
    }

    init() {
        self.x = 0
        self.y = 0
    }

    init(_ x: Int, _ y: Int) {
        self.x = x
        self.y = y
    }

    func manhattanDistance(to: Location) -> Int {
        return abs(self.x - to.x) + abs(self.y - to.y)
    }

    func getAdjacent() -> [Location] {
        return [
            Location(x + 1, y),
            Location(x, y + 1),
            Location(x - 1, y),
            Location(x, y - 1),
        ]
    }

    func getAdjacent(inBounds rect: Rectangle) -> [Location] {
        var result = [Location]()
        if x > rect.left {
            result.append(Location(x - 1, y))
        }
        if x < rect.right {
            result.append(Location(x + 1, y))
        }
        if y > rect.top {
            result.append(Location(x, y - 1))
        }
        if y < rect.bottom {
            result.append(Location(x, y + 1))
        }
        return result
    }

    func getAdjacent(_ direction: Direction) -> Location {
        switch direction {
        case .up: return Location(x, y - 1)
        case .down: return Location(x, y + 1)
        case .left: return Location(x - 1, y)
        case .right: return Location(x + 1, y)
        }
    }

    func isAdjacent(other: Location) -> Bool {
        if other.x == x {
            return abs(other.y - y) <= 1
        }
        if other.y == y {
            return abs(other.x - x) <= 1
        }
        return false
    }

    func getSurrounding() -> [Location] {
        return [
            Location(x + 1, y),
            Location(x + 1, y + 1),
            Location(x, y + 1),
            Location(x - 1, y + 1),
            Location(x - 1, y),
            Location(x - 1, y - 1),
            Location(x, y - 1),
            Location(x + 1, y - 1),
        ]
    }

    func getSurrounding(inBounds rect: Rectangle) -> [Location] {
        var result = [Location]()
        if x < rect.right {
            result.append(Location(x + 1, y))
        }
        if x < rect.right && y < rect.bottom {
            result.append(Location(x + 1, y + 1))
        }
        if y < rect.bottom {
            result.append(Location(x, y + 1))
        }
        if x > rect.left && y < rect.bottom {
            result.append(Location(x - 1, y + 1))
        }
        if x > rect.left {
            result.append(Location(x - 1, y))
        }
        if x > rect.left && y > rect.top {
            result.append(Location(x - 1, y - 1))
        }
        if y > rect.top {
            result.append(Location(x, y - 1))
        }
        if x < rect.right && y > rect.top {
            result.append(Location(x + 1, y - 1))
        }
        return result
    }

    func isSurrounding(other: Location) -> Bool {
        return abs(other.x - x) <= 1 && abs(other.y - y) <= 1
    }

    mutating func moveTo(_ x: Int, _ y: Int) {
        self.x = x
        self.y = y
    }

    mutating func move(inDirection dir: Direction) {
        switch dir {
        case .up: y -= 1
        case .down: y += 1
        case .left: x -= 1
        case .right: x += 1
        }
    }

    static func == (left: Location, right: Location) -> Bool {
        return left.x == right.x && left.y == right.y
    }
}
