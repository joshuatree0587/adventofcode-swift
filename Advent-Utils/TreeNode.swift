//
//  TreeNode.swift
//  Advent-Utils
//
//  Created by Joshua Stachowski on 12/8/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class TreeNode<T> {
    var value: T
    weak var parent: TreeNode?
    var children: [TreeNode]

    init(containing value: T) {
        self.value = value
        self.children = []
    }

    func addChild(_ node: TreeNode) {
        children.append(node)
        node.parent = self
    }

    func addChild(withValue value: T) -> TreeNode {
        let child = TreeNode(containing: value)
        children.append(child)
        child.parent = self
        return child
    }

    func printTree() {
        printTree("", true)
    }

    private func printTree(_ indent: String, _ last: Bool) {
        print("\(indent)\(last ? "└─" : "├─") \(value)")
        let newIndent = indent + (last ? "   " : "│  ")
        if children.count > 0 {
            for i in 0..<children.count {
                children[i].printTree(newIndent, i == children.count - 1)
            }
        }
    }
}
