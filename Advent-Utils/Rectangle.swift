//
//  Rectangle.swift
//  Advent-Utils
//
//  Created by Joshua Stachowski on 12/17/2022.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

struct Rectangle {
    var left: Int
    var top: Int
    var right: Int
    var bottom: Int

    var width: Int { return right - left + 1 }
    var height: Int { return bottom - top + 1 }
    var rangeX: ClosedRange<Int> { return left...right }
    var rangeY: ClosedRange<Int> { return top...bottom }

    init() {
        left = 0
        top = 0
        right = 0
        bottom = 0
    }

    init(width: Int, height: Int) {
        left = 0
        top = 0
        right = width - 1
        bottom = height - 1
    }
    
    init(containing points:[Location]) {
        left = Int.max
        top = Int.max
        right = Int.min
        bottom = Int.min
        for point in points {
            if point.x < left {
                left = point.x
            }
            if point.y < top {
                top = point.y
            }
            if point.x > right {
                right = point.x
            }
            if point.y > bottom {
                bottom = point.y
            }
        }
    }
    
    init(containing points:Set<Location>) {
        left = Int.max
        top = Int.max
        right = Int.min
        bottom = Int.min
        for point in points {
            if point.x < left {
                left = point.x
            }
            if point.y < top {
                top = point.y
            }
            if point.x > right {
                right = point.x
            }
            if point.y > bottom {
                bottom = point.y
            }
        }
    }

    func contains(_ loc: Location) -> Bool {
        return rangeX.contains(loc.x) && rangeY.contains(loc.y)
    }
    
    func makeQuadrants() -> [Rectangle] {
        var quad1 = Rectangle()
        quad1.left = self.left
        quad1.top = self.top
        quad1.right = self.left + self.width / 2 - 1
        quad1.bottom = self.top + self.height / 2 - 1
        
        var quad2 = Rectangle()
        quad2.left = quad1.right + 1
        quad2.top = self.top
        quad2.right = self.right
        quad2.bottom = quad1.bottom
        
        var quad3 = Rectangle()
        quad3.left = self.left
        quad3.top = quad1.bottom + 1
        quad3.right = quad1.right
        quad3.bottom = self.bottom
        
        var quad4 = Rectangle()
        quad4.left = quad2.left
        quad4.top = quad3.top
        quad4.right = self.right
        quad4.bottom = self.bottom
        
        var result = [Rectangle]()
        if quad1.width > 0 && quad1.height > 0 {
            result.append(quad1)
        }
        if quad2.width > 0 && quad2.height > 0 {
            result.append(quad2)
        }
        if quad3.width > 0 && quad3.height > 0 {
            result.append(quad3)
        }
        if quad4.width > 0 && quad4.height > 0 {
            result.append(quad4)
        }
        return result
    }
}
