//
//  Direction.swift
//  Advent-Utils
//
//  Created by Joshua Stachowski on 12/4/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

enum Direction: String {
    case left
    case up
    case right
    case down

    func turnLeft() -> Direction {
        switch self {
        case .left: return .down
        case .up: return .left
        case .right: return .up
        case .down: return .right
        }
    }

    func turnRight() -> Direction {
        switch self {
        case .left: return .up
        case .up: return .right
        case .right: return .down
        case .down: return .left
        }
    }

    func turnAround() -> Direction {
        switch self {
        case .left: return .right
        case .up: return .down
        case .right: return .left
        case .down: return .up
        }
    }
}
