//
//  Pathfinder.swift
//  Advent-Utils
//
//  Created by Joshua Stachowski on 12/17/2022.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Pathfinder<T> where T: Hashable {
    var visited: Set<T>

    private var pending: [Step<T>]
    private var currentStep: Step<T>?
    private var finalStep: Step<T>?

    var completed: Bool { return finalStep != nil }

    var numSteps: Int { return finalStep?.number ?? -1 }

    init() {
        visited = Set<T>()
        pending = []
    }

    func search(from start: T, to end: T, visit: (T) -> Void) {
        search(from: start, visit: visit, testComplete: { $0 == end })
    }
    
    func search(from start: T, visit: (T) -> Void) {
        search(from: start, visit: visit, testComplete: { (state: T) -> Bool in return false })
    }

    func search(from start: T, visit: (T) -> Void, testComplete: (T) -> Bool) {
        visited.removeAll()
        pending.removeAll()

        pending.append(Step(start))
        visited.insert(start)

        currentStep = nil
        finalStep = nil

        while pending.count > 0 {
            currentStep = pending.removeFirst()
            if testComplete((currentStep?.state)!) {
                if finalStep == nil {
                    finalStep = currentStep
                    break
                }
            } else {
                visit((currentStep?.state)!)
            }
        }
    }

    func push(_ state: T) {
        if !visited.contains(state) {
            let step = Step(state)
            if currentStep != nil {
                step.previous = currentStep
                step.number = currentStep!.number + 1
            }
            visited.insert(state)
            pending.append(step)
        }
    }

    private class Step<T> {
        var state: T
        var previous: Step?
        var number: Int

        init(_ state: T) {
            self.state = state
            self.number = 1
        }
    }
}
