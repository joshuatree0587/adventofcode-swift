//
//  KnotHash.swift
//  Advent-Utils
//
//  Created by Joshua Stachowski on 12/14/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class KnotHash {
    static func hash(lengths: [Int]) -> [Int] {
        var numbers = getNumbers()
        hash(&numbers, withLengths: lengths, overIterations: 1)
        return numbers
    }

    static func hash(text: String) -> String {
        var numbers = getNumbers()
        var lengths = text.unicodeScalars.map { Int($0.value) }
        lengths.append(contentsOf: [17, 31, 73, 47, 23])
        hash(&numbers, withLengths: lengths, overIterations: 64)
        let denseHash = compressHash(numbers)
        let hexForm = convertToHex(denseHash)
        return hexForm
    }

    private static func hash(
        _ numbers: inout [Int], withLengths lengths: [Int], overIterations iterations: Int
    ) {
        var remaining = iterations
        var position = 0
        var skip = 0

        while remaining > 0 {
            for length in lengths {
                reverseRange(list: &numbers, start: position, length: length)
                position += (length + skip) % numbers.count
                skip += 1
            }

            remaining -= 1
        }
    }

    private static func getNumbers() -> [Int] {
        var result: [Int] = []
        for index in 0..<256 {
            result.append(index)
        }
        return result
    }

    private static func reverseRange(list: inout [Int], start: Int, length: Int) {
        for index in 0..<(length / 2) {
            swapPositions(list: &list, first: start + index, second: start + length - 1 - index)
        }
    }

    private static func swapPositions(list: inout [Int], first: Int, second: Int) {
        let firstIndex = first % list.count
        let secondIndex = second % list.count

        let temp = list[firstIndex]
        list[firstIndex] = list[secondIndex]
        list[secondIndex] = temp
    }

    private static func compressHash(_ source: [Int]) -> [Int] {
        var result: [Int] = []
        for start in stride(from: 0, to: source.count, by: 16) {
            var entry = source[start]
            for index in (start + 1)..<(start + 16) {
                entry ^= source[index]
            }
            result.append(entry)
        }
        return result
    }

    private static func convertToHex(_ integers: [Int]) -> String {
        var result = ""
        for item in integers {
            result += String(format: "%02x", item)
        }
        return result
    }
}
