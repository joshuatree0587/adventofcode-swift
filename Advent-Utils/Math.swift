//
//  Math.swift
//  Advent-Utils
//
//  Created by Joshua Stachowski on 12/14/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

func gcd(_ a: Int, _ b: Int) -> Int {
    var (x, y) = (a, b)
    while y != 0 { (x, y) = (y, x % y) }
    return x
}

func gcd(_ list: [Int]) -> Int {
    return list.reduce(0, gcd)
}

func lcm(_ a: Int, _ b: Int) -> Int {
    return (a * b) / gcd(a, b)
}

func lcm(_ list: [Int]) -> Int {
    return list.reduce(1, lcm)
}
