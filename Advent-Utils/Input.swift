//
//  Input.swift
//  Advent-Utils
//
//  Created by Joshua Stachowski on 12/3/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Input {
    static func readText(from file: String, inDirectory path: String) -> String {
        var result: String?
        if let path = Bundle.main.path(forResource: file, ofType: "txt", inDirectory: path) {
            do {
                let contents = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
                result = contents
            } catch {
                print("Failed to read text from file: \(file)")
            }
        } else {
            print("Failed to load file from app bundle: \(file)")
        }
        return result!
    }

    static func readLines(from file: String, inDirectory path: String) -> [String] {
        let input = readText(from: file, inDirectory: path)
        var lines = input.components(separatedBy: "\n")
        if lines.count > 0 && lines.last!.isEmpty {
            _ = lines.popLast()
        }
        return lines
    }
}
