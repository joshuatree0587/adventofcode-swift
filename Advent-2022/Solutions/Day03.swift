//
//  Day03.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/3/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day03 {
    func execute() {
        print("-- Day 03 --")
        let lines = Input.readLines(from: "day03", inDirectory: "2022")
        partOne(lines)
        partTwo(lines)
    }

    func partOne(_ lines: [String]) {
        var priorities = 0

        for line in lines {
            let items = Array(line)
            let middleIndex = items.count / 2
            let firstHalf = Set(items[0...middleIndex - 1])
            let secondHalf = Set(items[middleIndex...])
            let overlap = firstHalf.intersection(secondHalf)
            priorities += getPriority(overlap.first!)
        }

        print("Part 1: Priorities = \(priorities)")
    }

    func partTwo(_ lines: [String]) {
        var priorities = 0

        for i in stride(from: 0, to: lines.count, by: 3) {
            let first = Set(lines[i])
            let second = Set(lines[i + 1])
            let third = Set(lines[i + 2])
            let overlap = first.intersection(second).intersection(third)
            priorities += getPriority(overlap.first!)
        }

        print("Part 2: Priorities = \(priorities)")
    }

    func getPriority(_ item: Character) -> Int {
        let ascii = item.asciiValue!
        let lowerCase = Character("a").asciiValue!
        let upperCase = Character("A").asciiValue!
        let result = (ascii >= lowerCase) ? ascii - lowerCase : ascii - upperCase + 26
        return Int(result + 1)
    }
}
