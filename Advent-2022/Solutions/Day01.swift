//
//  Day01.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/1/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day01 {
    func execute() {
        print("-- Day 01 --")
        let lines = Input.readLines(from: "day01", inDirectory: "2022")

        var calorieCounts: [Int] = []
        var currentCalories = 0

        for line in lines {
            if line.isEmpty {
                calorieCounts.append(currentCalories)
                currentCalories = 0
            } else {
                currentCalories += Int(line)!
            }
        }

        calorieCounts = calorieCounts.sorted()

        var topThree = 0
        for i in 1...3 {
            topThree += calorieCounts[calorieCounts.count - i]
        }

        print("Part 1: Top Calories = \(calorieCounts.last!)")
        print("Part 2: Top 3 Calories = \(topThree)")
    }
}
