//
//  Day16.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/30/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation
import Algorithms

class Day16_v1 {
    private var valveLookup = Dictionary<String, Valve>()
    private var valveCount = 0
    private var pathfinder = Pathfinder<Step>()
    private var finishedPaths = [Step]()
    
    func execute() {
        print("-- Day 16 --")
        for line in Input.readLines(from: "day16", inDirectory: "2022") {
            let valve = Valve(fromText: line)
            if valve.flowRate > 0 {
                valveCount += 1
            }
            valveLookup[valve.name] = valve
        }
        
        // Part 1
        pathfinder.search(from: Step(), visit: doSingle)
        let singleMax = finishedPaths.max(by: { $0.pressureReleased < $1.pressureReleased })
        print("Part 1: Pressure relieved = \(singleMax?.pressureReleased ?? 0)")
        
        // Part 2
//        finishedPaths = [Step]()
//        pathfinder.search(from: Step(), visit: doDouble)
//        let doubleMax = finishedPaths.max(by: { $0.pressure < $1.pressure })
//        print("Part 2: Pressure relieved = \(doubleMax?.pressure ?? 0)")
    }
    
    func doSingle(current: Step) {
        let next = simulateMinute(forStep: current, toDuration: 30)
        if next.minutesElapsed >= 30 {
            finishedPaths.append(next)
            return
        }
        let options = buildOptions(forWorker: 0, from: next)
        queueSteps(options)
    }
    
    func doDouble(current: Step) {
        let next = simulateMinute(forStep: current, toDuration: 26)
        if next.minutesElapsed >= 26 {
            finishedPaths.append(next)
            return
        }
        let selfOptions = buildOptions(forWorker: 0, from: next)
        for option in selfOptions {
            let helpOptions = buildOptions(forWorker: 1, from: option)
            queueSteps(helpOptions)
        }
    }
    
    func simulateMinute(forStep current: Step, toDuration maxTime: Int) -> Step {
        var next = current
        
        // If all the valves are open, just finish simulating this arrangement
        if next.openedValves.count == valveCount {
            next.pressureReleased += (maxTime - next.minutesElapsed) * next.flowRate
            next.minutesElapsed = maxTime
        }
        // Relieve some pressure
        else {
            next.pressureReleased += next.flowRate
            next.minutesElapsed += 1
        }
        
        return next
    }
    
    func buildOptions(forWorker worker: Int, from current: Step) -> [Step] {
        var options = [Step]()
        
        // If the valve at this location isn't opened, then open it
        // ... but only if the flow rate is > 0
        let valve = valveLookup[current.workerLocations[worker]]!
        if valve.flowRate > 0 && !current.openedValves.keys.contains(valve.name) {
            var next = current
            next.openedValves[valve.name] = current.minutesElapsed
            next.flowRate += valve.flowRate
            // Queue this room up again
            options.append(next)
        }
        
        // Queue up the next set of locations
        for destination in valve.connections {
            var next = current
            next.workerLocations[worker] = destination
            options.append(next)
        }
        
        return options
    }
    
    func queueSteps(_ options: [Step]) {
        for option in options {
            var approved = true
            // Don't bother queuing up less optimal paths than ones we've already explored
            for other in pathfinder.visited {
                if other.flowRate > option.flowRate && other.pressureReleased > option.pressureReleased && other.minutesElapsed < option.minutesElapsed {
                    approved = false
                    break
                }
            }
            if approved {
                pathfinder.push(option)
            }
        }
    }
    
    class Valve {
        var name: String
        var flowRate: Int
        var connections: ArraySlice<String>
        
        init(fromText text: String) {
            let terms = text
                .replacingOccurrences(of: "Valve ", with: "")
                .replacingOccurrences(of: " has flow rate=", with: ":")
                .replacingOccurrences(of: "; tunnels lead to valves ", with: ":")
                .replacingOccurrences(of: "; tunnel leads to valve ", with: ":")
                .replacingOccurrences(of: ", ", with: ":")
                .components(separatedBy: ":")
            self.name = terms[0]
            self.flowRate = Int(terms[1])!
            self.connections = terms.dropFirst(2)
        }
    }

    struct Step: Hashable, Equatable {
        var minutesElapsed = 0
        var pressureReleased = 0
        var flowRate = 0
        var workerLocations = ["AA", "AA"]
        var openedValves = [String: Int]()
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(workerLocations[0])
            hasher.combine(workerLocations[1])
            hasher.combine(flowRate)
            hasher.combine(openedValves.count)
        }
        
        static func == (lhs: Self, rhs: Self) -> Bool {
            return lhs.workerLocations[0] == rhs.workerLocations[0] &&
                   lhs.workerLocations[1] == rhs.workerLocations[1] &&
                   lhs.flowRate == rhs.flowRate &&
                   lhs.openedValves == rhs.openedValves
        }
    }
}

class Day16_v2 {
    private var valveLookup = Dictionary<String, Valve>()
    private var valveCount = 0
    private var pathfinder = Pathfinder<String>()
    
    func execute() {
        print("-- Day 16 --")
        for line in Input.readLines(from: "day16", inDirectory: "2022") {
            let valve = Valve(fromText: line)
            if valve.flowRate > 0 {
                valveCount += 1
            }
            valveLookup[valve.name] = valve
        }
        
        // Find the shortest routes from each valve to another valve
        for start in valveLookup.values {
            for finish in valveLookup.values {
                pathfinder.search(from: start.name, to: finish.name, visit: explore)
                start.travelTimes[finish.name] = pathfinder.numSteps
            }
        }
        
        // Part 1
        let state = State()
        state.minutesLeft = 30
        let worker = Worker()
        
        let (nextValve, travelTime) = findNextValve(forState: state, startingFrom: "AA")
        worker.destination = nextValve
        worker.travelTime = travelTime
        print("First destination = \(nextValve)")

        while state.minutesLeft > 0 {
            print("Minute \(30 - state.minutesLeft)")
            print("    Opened valves = \(state.openedValves)")
            state.pressureReleased += state.flowRate
            print("    Released pressure = \(state.flowRate) -> \(state.pressureReleased)")
            if worker.travelTime > 0 {
                worker.travelTime -= 1
                print("    Traveling to \(worker.destination). Travel time = \(worker.travelTime)")
            } else if state.openedValves.contains(worker.destination) {
                let (nextValve, travelTime) = findNextValve(forState: state, startingFrom: worker.destination)
                worker.destination = nextValve
                worker.travelTime = travelTime
                print("    New destination chosen = \(worker.destination). Travel time = \(worker.travelTime)")
            } else {
                state.openedValves.insert(worker.destination)
                state.flowRate += valveLookup[worker.destination]!.flowRate
                print("    Opening valve = \(worker.destination). Flow rate += \(valveLookup[worker.destination]!.flowRate) -> \(state.flowRate)")
            }
            state.minutesLeft -= 1
        }
        print("Part 1: Pressure released = \(state.pressureReleased)")
    }
    
    func explore(location: String) {
        let valve = valveLookup[location]!
        for tunnel in valve.connections {
            pathfinder.push(tunnel)
        }
    }
    
    func findNextValve(forState state: State, startingFrom location:String) -> (String, Int) {
        var bestScore = 0
        var bestValve = location
        var bestTime = 0
        for entry in valveLookup {
            if entry.value.flowRate > 0 && !state.openedValves.contains(entry.key) {
                let travelTime = valveLookup[location]!.travelTimes[entry.key]!
                let score = (state.minutesLeft - travelTime) * entry.value.flowRate
                print("Score is \(score) for valve \(entry.key) with a travel time of \(travelTime)")
                if score > bestScore {
                    bestScore = score
                    bestValve = entry.value.name
                    bestTime = travelTime
                }
            }
        }
        print("Winner = \(bestValve)")
        return (bestValve, bestTime)
    }
    
    class Valve {
        var name: String
        var flowRate: Int
        var connections: ArraySlice<String>
        var travelTimes: Dictionary<String, Int>
        
        init(fromText text: String) {
            let terms = text
                .replacingOccurrences(of: "Valve ", with: "")
                .replacingOccurrences(of: " has flow rate=", with: ":")
                .replacingOccurrences(of: "; tunnels lead to valves ", with: ":")
                .replacingOccurrences(of: "; tunnel leads to valve ", with: ":")
                .replacingOccurrences(of: ", ", with: ":")
                .components(separatedBy: ":")
            self.name = terms[0]
            self.flowRate = Int(terms[1])!
            self.connections = terms.dropFirst(2)
            self.travelTimes = Dictionary<String, Int>()
        }
    }
    
    class State {
        var minutesLeft = 0
        var openedValves = Set<String>()
        var flowRate = 0
        var pressureReleased = 0
    }
    
    class Worker {
        var travelTime: Int = 0
        var destination: String = "AA"
    }
    
    struct SearchResult {
        var valve: Valve
        var travelTime: Int
    }
}

class Day16_v3 {
    private var valveLookup = Dictionary<String, Valve>()
    private var valveCount = 0
    private var explorer = Pathfinder<String>()
    private var pathfinder = Pathfinder<Step>()
    private var finishedPaths = [Step]()
    
    func execute() {
        print("-- Day 16 --")
        for line in Input.readLines(from: "day16", inDirectory: "2022") {
            let valve = Valve(fromText: line)
            if valve.flowRate > 0 {
                valveCount += 1
            }
            valveLookup[valve.name] = valve
        }
        
        // Find the shortest routes from each valve to another valve
        for start in valveLookup.values {
            for finish in valveLookup.values {
                explorer.search(from: start.name, to: finish.name, visit: explore)
                start.travelTimes[finish.name] = explorer.numSteps
            }
        }
        
        // Part 1
        pathfinder.search(from: Step(withMinutes: 30), visit: doSingle)
        let singleMax = finishedPaths.max(by: { $0.pressureReleased < $1.pressureReleased })
        print("Part 1: Pressure released = \(singleMax?.pressureReleased ?? 0)")
//        if singleMax != nil {
//            for entry in singleMax!.actionLog {
//                print(entry)
//            }
//        }
    }
    
    func explore(location: String) {
        let valve = valveLookup[location]!
        for tunnel in valve.connections {
            explorer.push(tunnel)
        }
    }
    
    func doSingle(step: Step) {
        var step = step
        
        // Update the released pressure
        while step.minutesLeft > 0 && step.worker.travelTime > 0 {
            step.minutesLeft -= 1
            step.worker.travelTime -= 1
            step.pressureReleased += step.flowRate
            
//            step.actionLog.append("Minute \(30 - step.minutesLeft)")
//            step.actionLog.append("    Opened Valves = \(step.openedValves)")
//            step.actionLog.append("    Releasing Pressure = \(step.flowRate) -> \(step.pressureReleased)")
//
//            step.actionLog.append("    Worker moving to \(step.worker.destination). Remaining travel time = \(step.worker.travelTime)")
        }
        
        // Open the valve
        let location = step.worker.destination
        let currentValve = valveLookup[location]!
        if currentValve.flowRate > 0 {
//            step.actionLog.append("    Worker opening valve \(location).")
            
            step.openedValves.insert(location)
            step.flowRate += currentValve.flowRate
        }
        
        // If all the valves are now open, just finish simulating this arrangement
        if step.openedValves.count == valveCount {
            step.worker.destination = "--"
            
//            while step.minutesLeft > 0 {
//                step.minutesLeft -= 1
//                step.worker.travelTime -= 1
//                step.pressureReleased += step.flowRate
//
//                step.actionLog.append("Minute \(30 - step.minutesLeft)")
//                step.actionLog.append("    Opened Valves = \(step.openedValves)")
//                step.actionLog.append("    Releasing Pressure = \(step.flowRate) -> \(step.pressureReleased)")
//            }
            step.pressureReleased += step.minutesLeft * step.flowRate
            step.minutesLeft = 0
        }
        
        if step.minutesLeft == 0 {
            finishedPaths.append(step)
            return
        }
        
        // Queue up the next set of valves
        for destination in valveLookup.values {
            if destination.flowRate > 0 && !step.openedValves.contains(destination.name) {
                var next = step
                next.worker.destination = destination.name
                next.worker.travelTime = currentValve.travelTimes[destination.name]!
                
//                next.actionLog.append("    Next destination = \(next.worker.destination). Travel time = \(next.worker.travelTime)")
                
                queueStep(next)
            }
        }
    }
    
    func queueStep(_ option: Step) {
        var approved = true
        // Don't bother queuing up less optimal paths than ones we've already explored
        for other in pathfinder.visited {
            if other.flowRate > option.flowRate && other.pressureReleased > option.pressureReleased && other.minutesLeft > option.minutesLeft {
                approved = false
                break
            }
        }
        if approved {
            pathfinder.push(option)
        }
    }
    
    func doSingle_old(step: Step) {
        var step = step
        
        // Update the released pressure
        let elapsedTime = step.worker.travelTime
        step.minutesLeft -= elapsedTime
        step.pressureReleased += step.flowRate * elapsedTime
        
        // Open the valve
        let location = step.worker.destination
        let currentValve = valveLookup[location]!
        if currentValve.flowRate > 0 {
            step.openedValves.insert(location)
            step.flowRate += currentValve.flowRate
            step.minutesLeft -= 1
        }
        
        // If all the valves are now open, just finish simulating this arrangement
        if step.openedValves.count == valveCount {
            step.pressureReleased += step.minutesLeft * step.flowRate
            step.minutesLeft = 0
        }
        
        if step.minutesLeft == 0 {
            finishedPaths.append(step)
            return
        }
        
        // Queue up the next set of valves
        for destination in valveLookup.values {
            if destination.flowRate > 0 && !step.openedValves.contains(destination.name) {
                var next = step
                next.worker.destination = destination.name
                next.worker.travelTime = currentValve.travelTimes[destination.name]!
                pathfinder.push(next)
            }
        }
    }
    
    class Valve {
        var name: String
        var flowRate: Int
        var connections: ArraySlice<String>
        var travelTimes: Dictionary<String, Int>
        
        init(fromText text: String) {
            let terms = text
                .replacingOccurrences(of: "Valve ", with: "")
                .replacingOccurrences(of: " has flow rate=", with: ":")
                .replacingOccurrences(of: "; tunnels lead to valves ", with: ":")
                .replacingOccurrences(of: "; tunnel leads to valve ", with: ":")
                .replacingOccurrences(of: ", ", with: ":")
                .components(separatedBy: ":")
            self.name = terms[0]
            self.flowRate = Int(terms[1])!
            self.connections = terms.dropFirst(2)
            self.travelTimes = Dictionary<String, Int>()
        }
    }
    
    struct Step: Hashable, Equatable {
        var minutesLeft = 0
        var openedValves = Set<String>()
        var flowRate = 0
        var pressureReleased = 0
        var worker = Worker()
        var actionLog = [String]()
        
        init(withMinutes minutes: Int = 0) {
            self.minutesLeft = minutes
        }
    }
    
    struct Worker: Hashable {
        var travelTime: Int = 0
        var destination: String = "AA"
    }
}

class Day16_v4 {
    private var valveLookup = Dictionary<String, Valve>()
    private var usefulValves = [Valve]()
    private var explorer = Pathfinder<String>()
    
    func execute() {
        print("-- Day 16 --")
        for line in Input.readLines(from: "day16", inDirectory: "2022") {
            let valve = Valve(fromText: line)
            if valve.flowRate > 0 {
                usefulValves.append(valve)
            }
            valveLookup[valve.name] = valve
        }
        
        // Find the shortest routes from each valve to another valve
        for start in valveLookup.values {
            for finish in valveLookup.values {
                explorer.search(from: start.name, to: finish.name, visit: explore)
                start.travelTimes[finish.name] = explorer.numSteps
            }
        }
        
        // Part 1
        print("Part 1: Useful Valves = \(usefulValves.count)")
        print("Part 1: Permutations = \(usefulValves.permutations().count)")
    }
    
    func explore(location: String) {
        let valve = valveLookup[location]!
        for tunnel in valve.connections {
            explorer.push(tunnel)
        }
    }
    
    class Valve {
        var name: String
        var flowRate: Int
        var connections: ArraySlice<String>
        var travelTimes: Dictionary<String, Int>
        
        init(fromText text: String) {
            let terms = text
                .replacingOccurrences(of: "Valve ", with: "")
                .replacingOccurrences(of: " has flow rate=", with: ":")
                .replacingOccurrences(of: "; tunnels lead to valves ", with: ":")
                .replacingOccurrences(of: "; tunnel leads to valve ", with: ":")
                .replacingOccurrences(of: ", ", with: ":")
                .components(separatedBy: ":")
            self.name = terms[0]
            self.flowRate = Int(terms[1])!
            self.connections = terms.dropFirst(2)
            self.travelTimes = Dictionary<String, Int>()
        }
    }
}
