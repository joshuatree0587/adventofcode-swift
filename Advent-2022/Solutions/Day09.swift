//
//  Day09.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/10/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day09 {
    func execute() {
        print("-- Day 09 --")
        let lines = Input.readLines(from: "day09", inDirectory: "2022")
        var moves = [Move]()
        for line in lines {
            let terms = line.components(separatedBy: " ")
            let count = Int(terms[1])!
            switch terms[0] {
            case "U": moves.append(Move(dir: Direction.up, count: count))
            case "D": moves.append(Move(dir: Direction.down, count: count))
            case "L": moves.append(Move(dir: Direction.left, count: count))
            case "R": moves.append(Move(dir: Direction.right, count: count))
            default: break
            }
        }

        partOne(moves: moves)
        partTwo(moves: moves)
    }

    func partOne(moves: [Move]) {
        let rope = Rope(withKnotCount: 2)
        rope.simulate(moves: moves)
        print("Part 1: The tail visited \(rope.visited.count) locations")
    }

    func partTwo(moves: [Move]) {
        let rope = Rope(withKnotCount: 10)
        rope.simulate(moves: moves)
        print("Part 2: The tail visited \(rope.visited.count) locations")
    }

    class Move {
        var dir: Direction
        var count: Int

        init(dir: Direction, count: Int) {
            self.dir = dir
            self.count = count
        }
    }

    class Rope {
        var knots: [Location]
        var visited: Set<Location>

        init(withKnotCount knots: Int) {
            self.knots = []
            self.visited = Set<Location>([Location()])
            for _ in 0..<knots {
                self.knots.append(Location())
            }
        }

        func simulate(moves: [Move]) {
            for step in moves {
                for _ in 1...step.count {
                    move(inDirection: step.dir)
                }
            }
        }

        func move(inDirection dir: Direction) {
            // Move the head
            knots[0].move(inDirection: dir)

            // Wag the tail
            for i in 1..<knots.count {
                let prev = knots[i - 1]
                var next = knots[i]

                if !next.isSurrounding(other: prev) {
                    next.x += (prev.x - next.x).signum()
                    next.y += (prev.y - next.y).signum()

                    // Have to reassign because Location is a struct
                    knots[i] = next
                }
            }

            // Log the tail's position
            visited.insert(knots.last!)
        }
    }
}
