//
//  Day08.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/9/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day08 {
    var grid: [[Int]] = []
    var bounds = Rectangle()

    func execute() {
        print("-- Day 08 --")
        let lines = Input.readLines(from: "day08", inDirectory: "2022")

        bounds.bottom = lines.count - 1
        bounds.right = lines[0].count - 1
        grid = Array(repeating: Array(repeating: -1, count: bounds.height), count: bounds.width)

        // Populate the grid's data
        for y in bounds.rangeY {
            let heights = lines[y].map { Int(String($0))! }
            for x in bounds.rangeX {
                grid[x][y] = heights[x]
            }
        }

        // Search from every tree towards the forest's edges
        var numVisible = 0
        var bestScore = -1
        var bestLocation = Location(-1, -1)

        for x in bounds.rangeX {
            for y in bounds.rangeY {
                let location = Location(x, y)
                let (score, visible) = calcScenicScore(location)
                if score > bestScore {
                    bestScore = score
                    bestLocation = location
                }
                if visible {
                    numVisible += 1
                }
            }
        }

        print("Part 1: Visible trees = \(numVisible)")
        print("Part 2: The best score is \(bestScore) at \(bestLocation)")
    }

    func calcScenicScore(_ pos: Location) -> (Int, Bool) {
        let (distU, visibleU) = viewScenery(from: pos, facing: Direction.up)
        let (distD, visibleD) = viewScenery(from: pos, facing: Direction.down)
        let (distL, visibleL) = viewScenery(from: pos, facing: Direction.left)
        let (distR, visibleR) = viewScenery(from: pos, facing: Direction.right)

        return (distR * distL * distD * distU, visibleR || visibleL || visibleD || visibleU)
    }

    func viewScenery(from start: Location, facing dir: Direction) -> (Int, Bool) {
        let startHeight = grid[start.x][start.y]
        var pos = start
        var moves = 0
        var visible = true

        while true {
            let next = pos.getAdjacent(dir)
            if !bounds.contains(next) {
                break
            }

            pos = next
            moves += 1

            let height = grid[pos.x][pos.y]
            if height >= startHeight {
                visible = false
                break
            }
        }

        return (moves, visible)
    }

    // Old solution for Part 1

    func partOne_old() {
        var trees = Set<Location>()

        for x in bounds.rangeX {
            trees = findVisibleTrees(start: Location(x, bounds.top - 1), dir: Direction.down)
                .union(trees)
            trees = findVisibleTrees(start: Location(x, bounds.bottom + 1), dir: Direction.up)
                .union(trees)
        }

        for y in bounds.rangeY {
            trees = findVisibleTrees(start: Location(bounds.left - 1, y), dir: Direction.right)
                .union(trees)
            trees = findVisibleTrees(start: Location(bounds.right + 1, y), dir: Direction.left)
                .union(trees)
        }

        print("Part 1: Visible trees = \(trees.count)")
    }

    func findVisibleTrees(start: Location, dir: Direction) -> Set<Location> {
        var maxHeight = -1
        var pos = start.getAdjacent(dir)
        var result = Set<Location>()

        while bounds.contains(pos) {
            let height = grid[pos.x][pos.y]
            if height > maxHeight {
                result.insert(Location(pos.x, pos.y))
                maxHeight = height
            }
            pos.move(inDirection: dir)
        }

        return result
    }
}
