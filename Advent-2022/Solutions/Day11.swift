//
//  Day11.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/13/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import DequeModule
import Foundation

class Day11 {
    func execute() {
        print("-- Day 11 --")
        //let lines = IO.readLines(file: "day11", inDirectory: "2022")
        partOne()
        partTwo()
    }

    func partOne() {
        let monkeys = buildInputMonkeys()
        let sorted = simulate(monkeys: monkeys, rounds: 20, adjustWorry: { $0 / 3 })
        print("Part 1: Monkey business = \(sorted[0].inspected * sorted[1].inspected)")
    }

    func partTwo() {
        let monkeys = buildInputMonkeys()
        let mutiple = monkeys.reduce(1, { $0 * $1.divisor })
        let sorted = simulate(monkeys: monkeys, rounds: 10000, adjustWorry: { $0 % mutiple })
        print("Part 2: Monkey business = \(sorted[0].inspected * sorted[1].inspected)")
    }

    func simulate(monkeys: [Monkey], rounds: Int, adjustWorry: (UInt) -> UInt) -> [Monkey] {
        //for round in 1...rounds {
        for _ in 1...rounds {
            for monkey in monkeys {
                //print("Monkey \(monkey.id):")
                monkey.inspected += monkey.items.count
                for entry in monkey.items {
                    //print("  Monkey inspects an item with a worry level of \(item).")
                    var item = monkey.operation(entry)
                    //print("    Monkey gets bored with item. Worry level is divided by \(3) to \(item / 3).")
                    //item /= 3
                    item = adjustWorry(item)
                    if item % monkey.divisor == 0 {
                        //print("    Current worry level is divisible by \(monkey.divisor).")
                        //print("    Item with worry level \(item) is thrown to monkey \(monkey.targetT).")
                        monkeys[monkey.targetT].items.append(item)
                    } else {
                        //print("    Current worry level is not divisible by \(monkey.divisor).")
                        //print("    Item with worry level \(item) is thrown to monkey \(monkey.targetF).")
                        monkeys[monkey.targetF].items.append(item)
                    }
                }
                // Clear the items
                monkey.items = []
            }

            /*
            if [1, 20, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000].contains(round)
            {
                print("After round \(round):")
                for monkey in monkeys {
                    print("Monkey \(monkey.id): \(monkey.inspected)")
                    //print("Monkey \(monkey.id): \(Array(monkey.items))")
                }
            }
            */
        }

        /*
        print("After all rounds:")
        for monkey in monkeys {
            print("Monkey \(monkey.id) inspected items \(monkey.inspected) times.")
        }
        */

        return monkeys.sorted(by: { $0.inspected > $1.inspected })
    }

    func buildTestMonkeys() -> [Monkey] {
        let monkey0 = Monkey()
        monkey0.id = 0
        monkey0.items = [79, 98]
        monkey0.operation = { $0 * 19 }
        monkey0.divisor = 23
        monkey0.targetT = 2
        monkey0.targetF = 3

        let monkey1 = Monkey()
        monkey1.id = 1
        monkey1.items = [54, 65, 75, 74]
        monkey1.operation = { $0 + 6 }
        monkey1.divisor = 19
        monkey1.targetT = 2
        monkey1.targetF = 0

        let monkey2 = Monkey()
        monkey2.id = 2
        monkey2.items = [79, 60, 97]
        monkey2.operation = { $0 * $0 }
        monkey2.divisor = 13
        monkey2.targetT = 1
        monkey2.targetF = 3

        let monkey3 = Monkey()
        monkey3.id = 3
        monkey3.items = [74]
        monkey3.operation = { $0 + 3 }
        monkey3.divisor = 17
        monkey3.targetT = 0
        monkey3.targetF = 1

        return [monkey0, monkey1, monkey2, monkey3]
    }

    func buildInputMonkeys() -> [Monkey] {
        let monkey0 = Monkey()
        monkey0.id = 0
        monkey0.items = [93, 98]
        monkey0.operation = { $0 * 17 }
        monkey0.divisor = 19
        monkey0.targetT = 5
        monkey0.targetF = 3

        let monkey1 = Monkey()
        monkey1.id = 1
        monkey1.items = [95, 72, 98, 82, 86]
        monkey1.operation = { $0 + 5 }
        monkey1.divisor = 13
        monkey1.targetT = 7
        monkey1.targetF = 6

        let monkey2 = Monkey()
        monkey2.id = 2
        monkey2.items = [85, 62, 82, 86, 70, 65, 83, 76]
        monkey2.operation = { $0 + 8 }
        monkey2.divisor = 5
        monkey2.targetT = 3
        monkey2.targetF = 0

        let monkey3 = Monkey()
        monkey3.id = 3
        monkey3.items = [86, 70, 71, 56]
        monkey3.operation = { $0 + 1 }
        monkey3.divisor = 7
        monkey3.targetT = 4
        monkey3.targetF = 5

        let monkey4 = Monkey()
        monkey4.id = 4
        monkey4.items = [77, 71, 86, 52, 81, 67]
        monkey4.operation = { $0 + 4 }
        monkey4.divisor = 17
        monkey4.targetT = 1
        monkey4.targetF = 6

        let monkey5 = Monkey()
        monkey5.id = 4
        monkey5.items = [89, 87, 60, 78, 54, 77, 98]
        monkey5.operation = { $0 * 7 }
        monkey5.divisor = 2
        monkey5.targetT = 1
        monkey5.targetF = 4

        let monkey6 = Monkey()
        monkey6.id = 4
        monkey6.items = [69, 65, 63]
        monkey6.operation = { $0 + 6 }
        monkey6.divisor = 3
        monkey6.targetT = 7
        monkey6.targetF = 2

        let monkey7 = Monkey()
        monkey7.id = 4
        monkey7.items = [89]
        monkey7.operation = { $0 * $0 }
        monkey7.divisor = 11
        monkey7.targetT = 0
        monkey7.targetF = 2

        return [monkey0, monkey1, monkey2, monkey3, monkey4, monkey5, monkey6, monkey7]
    }

    class Monkey {
        var id: Int = 0
        var inspected: Int = 0

        var operation: (UInt) -> UInt = { $0 }

        var divisor: UInt = 0
        var targetT: Int = 0
        var targetF: Int = 0

        var items: [UInt] = []
    }
}
