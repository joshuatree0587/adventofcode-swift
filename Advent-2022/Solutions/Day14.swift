//
//  Day14.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/26/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day14 {
    func execute() {
        print("-- Day 14 --")
        let lines = Input.readLines(from: "day14", inDirectory: "2022")
        let cave = Cave()
        
        for line in lines {
            let points = line
                .replacingOccurrences(of: " -> ", with: ":")
                .components(separatedBy: ":")
                .map { makePoint(from: $0) }
            var previous = points.first!
            for point in points.dropFirst() {
                cave.placeRocks(from: previous, to: point)
                previous = point
            }
        }
        
        var iteration = 0
        while cave.dropSand(stopAtFloor: true) {
            iteration += 1
        }
        //print(cave)
        print("Part 1: Sand at rest = \(iteration)")
        
        while cave.dropSand(stopAtFloor: false) {
            iteration += 1
        }
        //print(cave)
        print("Part 2: Sand at rest = \(iteration + 1)")
    }
    
    func makePoint(from text: String) -> Location {
        let terms = text.components(separatedBy: ",")
        let x = Int(terms[0])!
        let y = Int(terms[1])!
        return Location(x, y)
    }
    
    class Cave: CustomStringConvertible {
        private var opening: Location
        private var grid: [Location: String]
        private var floorPos: Int
        
        var description: String {
            let bounds = Rectangle(containing: Array(grid.keys))
            var result = ""
            for y in bounds.rangeY {
                var line = ""
                for x in bounds.rangeX {
                    if let char = grid[Location(x, y)] {
                        line += char
                    } else {
                        line += " "
                    }
                }
                result += line + "\n"
            }
            return result
        }
        
        init() {
            opening = Location(500, 0)
            grid = [Location: String]()
            grid[opening] = "+"
            floorPos = 0
        }
        
        func placeRocks(from previous: Location, to next: Location) {
            for x in ClosedRange<Int>.makeAscending(previous.x, next.x) {
                for y in ClosedRange<Int>.makeAscending(previous.y, next.y) {
                    let point = Location(x, y)
                    grid[point] = "#"
                    if point.y > floorPos - 2 {
                        floorPos = point.y + 2
                    }
                }
            }
        }
        
        /// Returns whether to simulate another unit of sand
        func dropSand(stopAtFloor: Bool) -> Bool {
            var pos = opening
            while true {
                // Try to move down
                if let next = tryMove(pos, 0) {
                    pos = next
                }
                // Try to move down-left
                else if let next = tryMove(pos, -1) {
                    pos = next
                }
                // Try to move down-right
                else if let next = tryMove(pos, 1) {
                    pos = next
                }
                // Couldn't move away from the opening
                else if pos == opening {
                    return false
                }
                // Fall into the abyss
                else if pos.y >= floorPos - 1 && stopAtFloor {
                    return false
                }
                // Come to rest
                else {
                    grid[pos] = "."
                    return true
                }
            }
        }
        
        private func tryMove(_ pos: Location, _ dx: Int) -> Location? {
            // First, check if this is the floor
            if pos.y + 1 >= floorPos {
                return nil
            }
            // If not, then we'll check if there's anything else in the way
            let next = Location(pos.x + dx, pos.y + 1)
            if grid[next] == nil {
                return next
            } else {
                return nil
            }
        }
    }
}
