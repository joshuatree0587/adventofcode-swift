//
//  Day12.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/17/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day12 {
    var grid = [Location: UInt8]()
    var bounds = Rectangle()

    var start = Location()
    var finish = Location()
    let pathfinder = Pathfinder<Location>()

    var startOptions = [Location]()

    func execute() {
        print("-- Day 12 --")
        let lines = Input.readLines(from: "day12", inDirectory: "2022")

        bounds.bottom = lines.count - 1
        for y in bounds.rangeY {
            let row = Array(lines[y]).map { $0.asciiValue! }
            bounds.right = row.count - 1
            for x in bounds.rangeX {
                let loc = Location(x, y)
                if row[x] == Character("S").asciiValue {
                    start.moveTo(x, y)
                    grid[Location(x, y)] = Character("a").asciiValue
                    startOptions.append(loc)
                } else if row[x] == Character("E").asciiValue {
                    finish.moveTo(x, y)
                    grid[loc] = Character("z").asciiValue
                } else {
                    grid[loc] = row[x]
                    if row[x] == Character("a").asciiValue {
                        startOptions.append(loc)
                    }
                }
            }
        }

        partOne()
        partTwo()
    }

    func partOne() {
        pathfinder.search(from: start, to: finish, visit: visitLocation)
        print("Part 1: Fewest steps = \(pathfinder.numSteps)")
    }

    func partTwo() {
        var minSteps = Int.max
        var minStart = start
        for option in startOptions {
            pathfinder.search(from: option, to: finish, visit: visitLocation)
            if pathfinder.completed && pathfinder.numSteps < minSteps {
                minSteps = pathfinder.numSteps
                minStart = option
            }
        }
        print("Part 2: Fewest steps = \(minSteps) starting at \(minStart)")
    }

    func visitLocation(_ loc: Location) {
        let elevation = grid[loc]!
        for option in loc.getAdjacent(inBounds: bounds) {
            if grid[option]! <= elevation + 1 {
                pathfinder.push(option)
            }
        }
    }
}
