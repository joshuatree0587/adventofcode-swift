//
//  Day04.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/4/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day04 {
    func execute() {
        print("-- Day 04 --")
        let lines = Input.readLines(from: "day04", inDirectory: "2022")

        var overlappingPairs = 0
        var fullyContained = 0

        for line in lines {
            let trash = CharacterSet(charactersIn: ",-")
            let terms = line.components(separatedBy: trash).map { Int($0)! }

            let range1 = terms[0]...terms[1]
            let range2 = terms[2]...terms[3]

            if range1.encloses(range2) || range2.encloses(range1) {
                fullyContained += 1
            }

            if range1.overlaps(range2) {
                overlappingPairs += 1
            }
        }

        print("Part 1: Fully Contained = \(fullyContained)")
        print("Part 2: Overlapping Pairs = \(overlappingPairs)")
    }
}
