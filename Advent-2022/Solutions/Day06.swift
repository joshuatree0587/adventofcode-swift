//
//  Day06.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/6/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day06 {
    func execute() {
        print("-- Day 06 --")
        let lines = Input.readLines(from: "day06", inDirectory: "2022")
        let input = Array(lines.first!)

        let part1 = findStart(input: input, count: 4)
        print("Part 1: Start of packet marker = \(part1)")

        let part2 = findStart(input: input, count: 14)
        print("Part 2: Start of message marker = \(part2)")
    }

    func findStart(input: [Character], count: Int) -> Int {
        for i in (count - 1)...(input.count - 1) {
            if Set(input[(i - (count - 1))...i]).count == count {
                return i + 1
            }
        }
        return -1
    }
}
