//
//  Day15.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/26/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day15 {
    private var sensors = [Sensor]()
    
    func execute() {
        print("-- Day 15 --")
        let lines = Input.readLines(from: "day15", inDirectory: "2022")
        sensors = lines.map { Sensor(fromText: $0) }
        //partOne_brute()
        partOne_ranges()
        //partOne_hack()
        partTwo()
    }
    
    func partOne_brute() {
        let occupied = Set(sensors.flatMap { [$0.location, $0.beacon] })
        
        var bounds = Rectangle(containing: occupied)
        for sensor in sensors {
            bounds.left = min(bounds.left, sensor.location.x - sensor.distance)
            bounds.top = min(bounds.top, sensor.location.y - sensor.distance)
            bounds.right = max(bounds.right, sensor.location.x + sensor.distance)
            bounds.bottom = max(bounds.bottom, sensor.location.y + sensor.distance)
        }
        
        //printMap(sensors, bounds)
        
        let y = 2000000
        var count = 0
        for x in bounds.rangeX {
            let pos = Location(x, y)
            if !occupied.contains(pos) {
                for sensor in sensors {
                    let dist = sensor.location.manhattanDistance(to: pos)
                    if dist <= sensor.distance {
                        count += 1
                        break
                    }
                }
            }
        }
        
        print("Part 1: Positions that cannot contain a beacon = \(count)")
    }
    
    func partOne_ranges() {
        let row = 2000000
        
        // For each sensor...
        var ranges = [ClosedRange<Int>]()
        for sensor in sensors {
            // Find its coverage along the row (range)
            let dy = abs(sensor.location.y - row)
            let dx = sensor.distance - dy
            if dx >= 0 {
                ranges.append((sensor.location.x - dx)...(sensor.location.x + dx))
            }
        }
        
        // Sort the ranges, and add them together
        ranges.sort()
        var result = 0
        var cursor = ranges[0].lowerBound - 1
        for range in ranges {
            if range.upperBound > cursor {
                if range.lowerBound <= cursor {
                    // If they overlap, add the remainder
                    result += range.upperBound - cursor
                } else {
                    // If they don't overlap, then add the whole range
                    result += range.upperBound - range.lowerBound
                }
                // Move the cursor to the end of the range
                cursor = range.upperBound
            }
        }
        
        print("Part 1: Covered area = \(result)")
    }
    
    func partOne_hack() {
        // HUGE ASSUMPTION:
        // There will not be any gaps between the reach of all sensors
        let row = 2000000
        var start = Int.max
        var end = Int.min
        for sensor in sensors {
            let dy = abs(sensor.location.y - row)
            let dx = sensor.distance - dy
            if dx >= 0 {
                start = min(start, sensor.location.x - dx)
                end = max(end, sensor.location.x + dx)
            }
        }
        print("Part 1: Covered area = \(end - start)")
    }
    
    func partTwo() {
        let bounds = Rectangle(width: 4000001, height: 4000001)
        let result = searchInside(bounds)!
        print("Part 2: Distress beacon (\(result.left), \(result.top)) tuning frequency = \(result.left * 4000000 + result.top)")
    }
    
    func searchInside(_ bounds: Rectangle) -> Rectangle? {
        for quad in bounds.makeQuadrants() {
            if !enclosedBySensor(quad) {
                if quad.width == 1 && quad.height == 1 {
                    return quad
                } else if let test = searchInside(quad) {
                    return test
                }
            }
        }
        return nil
    }
    
    func enclosedBySensor(_ quad: Rectangle) -> Bool {
        for sensor in sensors {
            if sensor.encloses(quad) {
                return true
            }
        }
        return false
    }
    
    func printMap(_ sensors: [Sensor], _ bounds: Rectangle) {
        var everything = [Location: String]()
        for sensor in sensors {
            everything[sensor.location] = "S"
            everything[sensor.beacon] = "B"
        }
        for y in bounds.rangeY {
            var line = ""
            for x in bounds.rangeX {
                if let char = everything[Location(x, y)] {
                    line += char
                } else {
                    line += " "
                }
            }
            print(line)
        }
    }
    
    class Sensor {
        var location: Location
        var beacon: Location
        var distance: Int
        
        init(fromText text: String) {
            let terms = text
                .replacingOccurrences(of: "Sensor at x=", with: "")
                .replacingOccurrences(of: ": closest beacon is at x=", with: ",")
                .replacingOccurrences(of: ", y=", with: ",")
                .components(separatedBy: ",")
            location = Location(Int(terms[0])!, Int(terms[1])!)
            beacon = Location(Int(terms[2])!, Int(terms[3])!)
            distance = location.manhattanDistance(to: beacon)
        }
        
        func encloses(_ rect: Rectangle) -> Bool {
            return contains(Location(rect.left, rect.top)) &&
                   contains(Location(rect.right, rect.top)) &&
                   contains(Location(rect.left, rect.bottom)) &&
                   contains(Location(rect.right, rect.bottom))
        }
        
        func contains(_ point: Location) -> Bool {
            return location.manhattanDistance(to: point) <= distance
        }
    }
}
