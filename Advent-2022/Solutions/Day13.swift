//
//  Day13.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/17/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day13 {
    var packets = [Packet]()

    func execute() {
        print("-- Day 13 --")
        let lines = Input.readLines(from: "day13", inDirectory: "2022")
        packets = lines.compactMap { parse($0) }
        partOne()
        partTwo()
    }

    func partOne() {
        var sum = 0
        for i in stride(from: 0, to: packets.count, by: 2) {
            let index = (i / 2) + 1
            //print("Pair \(index)")

            let left = packets[i]
            //print("- Left = \(left)")

            let right = packets[i + 1]
            //print("- Right = \(right)")

            let comp = compare(left, right)
            //print("comparison = \(comp))

            if comp == 1 {
                sum += index
            }
        }

        print("Part 1: Sum of correct pair indices: \(sum)")
    }

    func partTwo() {
        let dividerA = parse("[[2]]")!
        dividerA.isDivider = true
        packets.append(dividerA)
        let dividerB = parse("[[6]]")!
        dividerB.isDivider = true
        packets.append(dividerB)

        let sorted = packets.sorted(by: { compare($0, $1) == 1 })
        var divIndices = [Int]()

        for i in 0..<sorted.count {
            //print(sorted[i])
            if sorted[i].isDivider {
                divIndices.append(i + 1)
            }
        }

        print("Part 2: Divider packets at indices: \(divIndices[0]) * \(divIndices[1]) = \(divIndices[0] * divIndices[1])")
    }

    func parse(_ line: String) -> Packet? {
        if line.isEmpty {
            return nil
        } else {
            var offset = 1
            return parse(characters: Array(line), fromOffset: &offset)
        }
    }

    func parse(characters input: [Character], fromOffset offset: inout Int) -> Packet {
        var integer = -1
        var items = [Packet]()

        while offset < input.count {
            let char = input[offset]
            if let number = char.wholeNumberValue {
                // This char is a number. Let's update our integer to the new value.
                if integer == -1 {
                    integer = number
                } else {
                    integer = (integer * 10) + number
                }

                offset += 1
            } else {
                // This char is not a number.
                // If we previously built a number, store it now!
                if integer != -1 {
                    items.append(Packet(integer))
                    integer = -1
                }

                // Whatever happens next, we need to bump the offset
                offset += 1

                if char == "[" {
                    // Delving into another packet...
                    let item = parse(characters: input, fromOffset: &offset)
                    items.append(item)
                } else if char == "]" {
                    // We're done building this packet. Return it now.
                    return Packet(items)
                }  // else, it could be ","
            }
        }

        // This is actually unreachable, but it's required to be here anyway.
        return Packet(-1)
    }

    func compare(_ left: Packet, _ right: Packet) -> Int {
        //print("compare: \(left) to \(right)")
        var result = 0
        if left.type == .integer && right.type == .integer {
            result = (right.integer - left.integer).signum()
        } else if left.type == .array && right.type == .integer {
            result = compare(left, Packet([right]))
        } else if left.type == .integer && right.type == .array {
            result = compare(Packet([left]), right)
        } else if left.type == .array && right.type == .array {
            let max = max(left.items.count, right.items.count)
            for i in 0..<max {
                if left.items.count > i && right.items.count > i {
                    result = compare(left.items[i], right.items[i])
                    if result != 0 {
                        return result
                    }
                } else if left.items.count <= i {
                    return 1
                } else if right.items.count <= i {
                    return -1
                }
            }
        }
        return result
    }

    class Packet: CustomStringConvertible {
        var type: PacketType
        var items: [Packet]
        var integer: Int

        // Added for Part 2
        var isDivider: Bool

        var description: String {
            switch type {
            case .integer:
                return integer.description
            case .array:
                return items.description
            }
        }

        init(_ items: [Packet]) {
            self.type = .array
            self.items = items
            self.integer = -1
            self.isDivider = false
        }

        init(_ integer: Int) {
            self.type = .integer
            self.items = []
            self.integer = integer
            self.isDivider = false
        }
    }

    enum PacketType {
        case array
        case integer
    }
}
