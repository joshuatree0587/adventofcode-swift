//
//  Day10.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/11/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day10 {
    func execute() {
        print("-- Day 10 --")
        let lines = Input.readLines(from: "day10", inDirectory: "2022")
        var operations = [Op]()
        for line in lines {
            let terms = line.components(separatedBy: " ")
            let op = Op(code: terms[0])
            if op.code == "addx" {
                op.value = Int(terms[1])!
            }
            operations.append(op)
        }

        var cycles = 1
        var cursor = 0
        var register = 1
        var history = [register]
        var output = Array(repeating: " ", count: 240)

        while cursor < operations.count {
            history.append(register)
            let op = operations[cursor]

            //print("\(cycles): \(register): \(op)")

            if ((register - 1)...(register + 1)).contains(cycles % 40 - 1) {
                output[cycles - 1] = "#"
            }

            if op.elapsed == op.cycles - 1 {
                switch op.code {
                case "addx":
                    register += op.value
                default:
                    break
                }
                cursor += 1
            } else {
                op.elapsed += 1
            }

            cycles += 1
        }

        let result = history[ 20] *  20 +
                     history[ 60] *  60 +
                     history[100] * 100 +
                     history[140] * 140 +
                     history[180] * 180 +
                     history[220] * 220

        print("Part 1: Signal strength = \(result)")

        print("Part 2: The display looks like this:")
        print(output[0...39].joined())
        print(output[40...79].joined())
        print(output[80...119].joined())
        print(output[120...159].joined())
        print(output[160...199].joined())
        print(output[200...239].joined())
    }

    class Op: CustomStringConvertible {
        var code: String
        var value: Int
        var cycles: Int
        var elapsed: Int

        var description: String {
            return "\(code) \(value)"
        }

        init(code: String) {
            self.code = code
            self.value = 0
            self.elapsed = 0

            switch code {
            case "addx":
                self.cycles = 2
            case "noop":
                self.cycles = 1
            default:
                self.cycles = 0
            }
        }
    }
}
