//
//  Day07.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/8/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day07 {
    func execute() {
        print("-- Day 07 --")
        let lines = Input.readLines(from: "day07", inDirectory: "2022")
        let rootNode = TreeNode(containing: FileRecord(terms: ["dir", "/"]))
        var currentNode = rootNode
        var allDirectories = [rootNode.value]

        for line in lines {
            let terms = line.components(separatedBy: " ")
            if terms[0] == "$" {
                if terms[1] == "cd" {
                    switch terms[2] {
                    case "/": currentNode = rootNode
                    case "..": currentNode = currentNode.parent ?? rootNode
                    default:
                        currentNode = currentNode.children.first(where: {
                            $0.value.name == terms[2]
                        })!
                    }
                }
            } else {
                let record = FileRecord(terms: terms)
                if record.isDirectory { allDirectories.append(record) }
                _ = currentNode.addChild(withValue: record)
            }
        }

        calcSize(node: rootNode)
        //rootNode.printTree()

        partOne(directories: allDirectories)
        partTwo(directories: allDirectories)
    }

    func calcSize(node: TreeNode<FileRecord>) {
        if node.value.isDirectory {
            node.value.size = 0
            for child in node.children {
                calcSize(node: child)
                node.value.size += child.value.size
            }
        }
    }

    func partOne(directories: [FileRecord]) {
        var result = 0
        for record in directories {
            if record.size <= 100000 {
                result += record.size
            }
        }
        print("Part 1 : Total directory sizes = \(result)")
    }

    func partTwo(directories: [FileRecord]) {
        let spaceUsed = directories[0].size
        let spaceFree = 70_000_000 - spaceUsed
        let spaceNeeded = 30_000_000 - spaceFree
        var result = directories[0]
        for record in directories {
            if record.size >= spaceNeeded && record.size < result.size {
                result = record
            }
        }
        print("Part 2 : Directory to delete = \(result.name) (dir, size=\(result.size))")
    }

    class FileRecord: CustomStringConvertible {
        var name: String
        var isDirectory: Bool
        var size: Int

        var description: String {
            return "\(name) (\(isDirectory ? "dir" : "file"), size=\(size))"
        }

        init(terms: [String]) {
            name = terms[1]
            isDirectory = terms[0] == "dir"
            size = isDirectory ? 0 : Int(terms[0])!
        }
    }
}
