//
//  Day05.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/5/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day05 {
    //     [D]
    // [N] [C]
    // [Z] [M] [P]
    //  1   2   3
    //                      <- empty line
    // move 1 from 2 to 1
    // move 3 from 1 to 3
    // move 2 from 2 to 1
    // move 1 from 1 to 2

    func execute() {
        print("-- Day 05 --")
        let lines = Input.readLines(from: "day05", inDirectory: "2022")

        let emptyLine = lines.firstIndex(where: { $0.isEmpty })!
        let stackBase = emptyLine - 2
        let stackInput = lines[0...stackBase].map(Array.init)
        let stackCount = (lines[emptyLine - 1].count / 4) + 1
        var itemStacks1 = Array(repeating: [Character](), count: stackCount)
        var itemStacks2 = Array(repeating: [Character](), count: stackCount)

        for c in 0...stackCount {
            for r in 0...stackBase {
                let i = (c * 4) + 1
                let line = stackInput[stackBase - r]
                let value = line.count > i ? line[i] : " "

                if value.isWhitespace { break }

                itemStacks1[c].append(value)
                itemStacks2[c].append(value)
            }
        }

        //var scratch = [Character]()
        for i in (emptyLine + 1)...(lines.count - 1) {
            let terms = lines[i].components(separatedBy: " ")
            let moves = Int(terms[1])!
            let source = Int(terms[3])! - 1
            let target = Int(terms[5])! - 1

            // Part 1
            for _ in 0..<moves {
                let item = itemStacks1[source].popLast()!
                itemStacks1[target].append(item)
            }

            /*
            let stack1 = itemStacks1[source]
            let items1 = stack1[(stack1.count - moves)...(stack1.count - 1)]
            itemStacks1[source] = stack1.dropLast(moves)
            itemStacks1[target].append(contentsOf: items1.reversed())
             */

            // Part 2
            /*
            for _ in 0..<moves {
                let item = itemStacks2[source].popLast()!
                scratch.append(item)
            }
            for _ in 0..<moves {
                let item = scratch.popLast()!
                itemStacks2[target].append(item)
            }
             */

            let stack2 = itemStacks2[source]
            let items2 = stack2[(stack2.count - moves)...(stack2.count - 1)]
            itemStacks2[source] = stack2.dropLast(moves)
            itemStacks2[target].append(contentsOf: items2)
        }

        printResult(prefix: "Part 1 : Top Crates = ", stacks: itemStacks1)
        printResult(prefix: "Part 2 : Top Crates = ", stacks: itemStacks2)
    }

    func printResult(prefix: String, stacks: [[Character]]) {
        var result = prefix
        for stack in stacks {
            result += String(stack.last!)
        }
        print(result)
    }
}
