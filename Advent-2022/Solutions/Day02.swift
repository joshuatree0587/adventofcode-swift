//
//  Day02.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 12/3/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day02 {
    func execute() {
        print("-- Day 02 --")
        let lines = Input.readLines(from: "day02", inDirectory: "2022")
        partOneNew(lines)
        partTwoNew(lines)
    }

    func partOneNew(_ lines: [String]) {
        var finalScore = 0

        for line in lines {
            switch line {
            case "A X": finalScore += 1 + 3
            case "A Y": finalScore += 2 + 6
            case "A Z": finalScore += 3 + 0
            case "B X": finalScore += 1 + 0
            case "B Y": finalScore += 2 + 3
            case "B Z": finalScore += 3 + 6
            case "C X": finalScore += 1 + 6
            case "C Y": finalScore += 2 + 0
            case "C Z": finalScore += 3 + 3
            default: break
            }
        }

        print("Part 1: Final Score = \(finalScore)")
    }

    func partTwoNew(_ lines: [String]) {
        var finalScore = 0

        for line in lines {
            switch line {
            case "A X": finalScore += 3 + 0
            case "A Y": finalScore += 1 + 3
            case "A Z": finalScore += 2 + 6
            case "B X": finalScore += 1 + 0
            case "B Y": finalScore += 2 + 3
            case "B Z": finalScore += 3 + 6
            case "C X": finalScore += 2 + 0
            case "C Y": finalScore += 3 + 3
            case "C Z": finalScore += 1 + 6
            default: break
            }
        }

        print("Part 2: Final Score = \(finalScore)")
    }

    func partOne_old(_ lines: [String]) {
        var finalScore = 0

        for line in lines {
            let terms = line.components(separatedBy: " ")
            switch terms[0] {
            case "A":  // Rock
                switch terms[1] {
                case "X":  // Rock - Draw
                    finalScore += 1 + 3
                case "Y":  // Paper - Win
                    finalScore += 2 + 6
                default:  // Scissors - Loss
                    finalScore += 3 + 0
                }
            case "B":  // Paper
                switch terms[1] {
                case "X":  // Rock - Loss
                    finalScore += 1 + 0
                case "Y":  // Paper - Draw
                    finalScore += 2 + 3
                default:  // Scissors - Win
                    finalScore += 3 + 6
                }
            default:  // Scissors
                switch terms[1] {
                case "X":  // Rock - Win
                    finalScore += 1 + 6
                case "Y":  // Paper - Loss
                    finalScore += 2 + 0
                default:  // Scissors - Draw
                    finalScore += 3 + 3
                }
            }
        }

        print("Part 1: Final Score = \(finalScore)")
    }

    func partTwo_old(_ lines: [String]) {
        var finalScore = 0

        for line in lines {
            let terms = line.components(separatedBy: " ")
            switch terms[0] {
            case "A":  // Rock
                switch terms[1] {
                case "X":  // We need to lose. Choose Scissors.
                    finalScore += 3 + 0
                case "Y":  // We need to draw. Choose Rock.
                    finalScore += 1 + 3
                default:  // We need to win. Choose Paper.
                    finalScore += 2 + 6
                }
            case "B":  // Paper
                switch terms[1] {
                case "X":  // We need to lose. Choose Rock.
                    finalScore += 1 + 0
                case "Y":  // We need to draw. Choose Paper.
                    finalScore += 2 + 3
                default:  // We need to win. Choose Scissors.
                    finalScore += 3 + 6
                }
            default:  // Scissors
                switch terms[1] {
                case "X":  // We need to lose. Choose Paper.
                    finalScore += 2 + 0
                case "Y":  // We need to draw. Choose Scissors.
                    finalScore += 3 + 3
                default:  // We need to win. Choose Rock.
                    finalScore += 1 + 6
                }
            }
        }

        print("Part 2: Final Score = \(finalScore)")
    }
}
