//
//  Day17.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 7/4/23.
//  Copyright © 2023 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day17 {
    func execute() {
        print("-- Day 17 --")
        for line in Input.readLines(from: "day17", inDirectory: "2022") {
            print(line)
        }
        print("-- Done --")
    }
}
