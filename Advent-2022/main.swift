//
//  main.swift
//  Advent-2022
//
//  Created by Joshua Stachowski on 11/31/22.
//  Copyright © 2022 Joshua Stachowski. All rights reserved.
//

import Foundation

let numberFormatter = NumberFormatter()
numberFormatter.numberStyle = .decimal
let startTime = CFAbsoluteTimeGetCurrent()

Day01().execute()
Day02().execute()
Day03().execute()
Day04().execute()
Day05().execute()
Day06().execute()
Day07().execute()
Day08().execute()
Day09().execute()
Day10().execute()
Day11().execute()
Day12().execute()
Day13().execute()
Day14().execute()
Day15().execute()
Day16_v1().execute()
Day17().execute()

let endTime = CFAbsoluteTimeGetCurrent()
let deltaTime = (endTime - startTime) * 1000
print("Time elapsed: \(numberFormatter.string(from: NSNumber(value: deltaTime))!) milliseconds.")
