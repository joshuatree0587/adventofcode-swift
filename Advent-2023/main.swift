//
//  main.swift
//  Advent-2023
//
//  Created by Joshua Stachowski on 12/17/23.
//  Copyright © 2023 Joshua Stachowski. All rights reserved.
//

import Foundation

let numberFormatter = NumberFormatter()
numberFormatter.numberStyle = .decimal
let startTime = CFAbsoluteTimeGetCurrent()

Day01().execute()

let endTime = CFAbsoluteTimeGetCurrent()
let deltaTime = (endTime - startTime) * 1000
print("Time elapsed: \(numberFormatter.string(from: NSNumber(value: deltaTime))!) milliseconds.")
