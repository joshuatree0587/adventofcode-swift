//
//  Day01.swift
//  Advent-2023
//
//  Created by Joshua Stachowski on 12/17/23.
//  Copyright © 2023 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day01 {
    func execute() {
        print("-- Day 01 --")
        
        //let lines = ["1abc2", "pqr3stu8vwx", "a1b2c3d4e5f", "treb7uchet"]
        let lines = Input.readLines(from: "day01", inDirectory: "2023")
        var total = 0

        for line in lines {
            var calibrationValue = 0
            
            // find the first digit
            for char in line {
                if char.isWholeNumber {
                    calibrationValue += char.wholeNumberValue! * 10
                    break
                }
            }
            
            // find the last digit
            for char in line.reversed() {
                if char.isWholeNumber {
                    calibrationValue += char.wholeNumberValue!
                    break
                }
            }
            
            // add the calibration value to the running total
            total += calibrationValue
        }

        print("Part 1: Sum of Calibration Values = \(total)")
    }
}
