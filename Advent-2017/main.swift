//
//  main.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 11/30/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

let numberFormatter = NumberFormatter()
numberFormatter.numberStyle = .decimal
let startTime = CFAbsoluteTimeGetCurrent()

Day01().execute()
Day02().execute()
Day03().execute()
Day04().execute()
Day05().execute()
Day06().execute()
Day07().execute()
Day08().execute()
Day09().execute()
Day10().execute()
Day11().execute()
Day12().execute()
Day13().execute()
Day14().execute()
Day15().execute()
Day16().execute()
Day17().execute()
Day18().execute()
Day19().execute()
Day20().execute()
Day21().execute()
Day22().execute()
Day23().execute()

let endTime = CFAbsoluteTimeGetCurrent()
let deltaTime = (endTime - startTime) * 1000
print("Time elapsed: \(numberFormatter.string(from: NSNumber(value: deltaTime))!) milliseconds.")
