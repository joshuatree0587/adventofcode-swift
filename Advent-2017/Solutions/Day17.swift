//
//  Day17.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/17/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day17 {
    let input = 382

    func execute() {
        print("-- Day 17 --")
        partOne()
        partTwo()
    }

    func partOne() {
        var buffer = [0]
        var position = 0

        for iter in 1...2017 {
            position = ((position + input) % buffer.count) + 1
            buffer.insert(iter, at: position)
        }

        let index = buffer.firstIndex(of: 2017)!
        let result = buffer[(index + 1) % buffer.count]

        print("The number following 2017 is \(result)")
    }

    func partTwo() {
        var position = 0
        var result = 0

        for iter in 1...50_000_000 {
            position = ((position + input) % iter) + 1
            if position == 1 {
                result = iter
            }
        }

        print("The number following 0 is \(result)")
    }
}
