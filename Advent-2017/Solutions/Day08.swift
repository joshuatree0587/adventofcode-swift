//
//  Day08.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/8/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day08 {
    func execute() {
        print("-- Day 08 --")
        let lines = Input.readLines(from: "day08", inDirectory: "2017")
        let instructions = lines.map { Instruction(data: $0) }

        var registers: [String: Int] = [:]

        for instruction in instructions {
            registers[instruction.register] = 0
            registers[instruction.condition.register] = 0
        }

        var largest = 0
        for instruction in instructions {
            instruction.execute(registers: &registers)
            largest = max(largest, registers[instruction.register]!)
        }

        /*
        for register in registers
        {
            print("Register \(register.key) has value \(register.value)")
        }
         */

        print("The largest register ever was \(largest)")
        print("The largest register is now \(registers.values.max()!)")
    }

    class Instruction {
        var register: String
        var action: String
        var value: Int
        var condition: Condition

        init(data: String) {
            let terms = data.components(separatedBy: " ")
            register = terms[0]
            action = terms[1]
            value = Int(terms[2])!
            condition = Condition(register: terms[4], operation: terms[5], value: Int(terms[6])!)
        }

        func execute(registers: inout [String: Int]) {
            if condition.evaluate(registers: registers) {
                let current = registers[register]!
                switch action {
                case "inc": registers[register] = current + value
                case "dec": registers[register] = current - value
                default: break
                }
            }
        }
    }

    class Condition {
        var register: String
        var operation: String
        var value: Int

        init(register: String, operation: String, value: Int) {
            self.register = register
            self.operation = operation
            self.value = value
        }

        func evaluate(registers: [String: Int]) -> Bool {
            let current = registers[register]!
            switch operation {
            case ">": return current > value
            case "<": return current < value
            case ">=": return current >= value
            case "<=": return current <= value
            case "==": return current == value
            case "!=": return current != value
            default: return false
            }
        }
    }
}
