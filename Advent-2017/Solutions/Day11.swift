//
//  Day11.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/10/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day11 {
    func execute() {
        print("-- Day 11 --")
        let input = Input.readText(from: "day11", inDirectory: "2017")

        var furthest = 0
        let location = Location()
        for move in input.components(separatedBy: ",") {
            switch move {
            case "n":
                location.y += 1
                location.z -= 1
            case "ne":
                location.x += 1
                location.z -= 1
            case "se":
                location.x += 1
                location.y -= 1
            case "s":
                location.y -= 1
                location.z += 1
            case "sw":
                location.z += 1
                location.x -= 1
            case "nw":
                location.y += 1
                location.x -= 1
            default: break
            }

            furthest = max(furthest, location.distance)
        }

        print("Distance = \(location.distance)")
        print("Furthest = \(furthest)")
    }

    class Location {
        var x = 0
        var y = 0
        var z = 0

        var distance: Int {
            return max(abs(x), abs(y), abs(x))
        }
    }
}
