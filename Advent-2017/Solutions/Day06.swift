//
//  Day06.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/6/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day06 {
    //let input = [0, 2, 7, 0]
    let input = [14, 0, 15, 12, 11, 11, 3, 5, 1, 6, 8, 4, 9, 1, 8, 4]

    func execute() {
        print("-- Day 06 --")
        var banks = input
        var visited = [hashState(state: input): 0]

        while true {
            var cursor = findStart(banks: banks)

            var remaining = banks[cursor]
            banks[cursor] = 0
            cursor = (cursor + 1) % banks.count

            while remaining > 0 {
                banks[cursor] += 1
                remaining -= 1
                cursor = (cursor + 1) % banks.count
            }

            let state = hashState(state: banks)
            if let match = visited[state] {
                print("Infinate loop found after \(visited.count) redistribution cycles")
                print("infinate loop spans for \(visited.count - match) redistribution cycles")
                break
            } else {
                visited[state] = visited.count
            }
        }
    }

    func findStart(banks: [Int]) -> Int {
        var result = 0
        var max = 0
        for index in 0..<banks.count {
            if banks[index] > max {
                max = banks[index]
                result = index
            }
        }
        return result
    }

    func hashState(state: [Int]) -> Int {
        var hasher = Hasher()
        for value in state {
            hasher.combine(value.hashValue)
        }
        return hasher.finalize()
    }
}
