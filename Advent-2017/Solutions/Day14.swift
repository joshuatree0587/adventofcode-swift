//
//  Day14.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/14/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day14 {
    let input = "ffayrhll"
    let bounds = Rectangle(width: 128, height: 128)

    func execute() {
        print("-- Day 14 --")
        partOne()
        partTwo()
    }

    func partOne() {
        var total = 0
        for row in bounds.rangeY {
            let name = input + "-" + String(row)
            let hash = KnotHash.hash(text: name)
            for char in hash {
                switch char {
                case "0": total += 0  // 0000
                case "1": total += 1  // 0001
                case "2": total += 1  // 0010
                case "3": total += 2  // 0011
                case "4": total += 1  // 0100
                case "5": total += 2  // 0101
                case "6": total += 2  // 0110
                case "7": total += 3  // 0111
                case "8": total += 1  // 1000
                case "9": total += 2  // 1001
                case "a": total += 2  // 1010
                case "b": total += 3  // 1011
                case "c": total += 2  // 1100
                case "d": total += 3  // 1101
                case "e": total += 3  // 1110
                case "f": total += 4  // 1111
                default: fatalError()
                }
            }
        }
        print("\(total) squares are used")
    }

    var grid = [Location: Bool]()
    var visited = Set<Location>()

    func partTwo() {
        for row in bounds.rangeY {
            var column = 0
            let name = input + "-" + String(row)
            let hash = KnotHash.hash(text: name)
            for char in hash {
                insert(char, row, column)
                column += 4
            }
        }

        var regions = 0
        for row in bounds.rangeY {
            for column in bounds.rangeX {
                let location = Location(row, column)
                if grid[location]! && !visited.contains(location) {
                    regions += 1
                    traverse(location)
                }
            }
        }

        print("There are \(regions) separate regions")
    }

    func traverse(_ start: Location) {
        visited.insert(start)
        for adjacent in start.getAdjacent(inBounds: bounds) {
            if grid[adjacent]! && !visited.contains(adjacent) {
                traverse(adjacent)
            }
        }
    }

    func insert(_ char: Character, _ row: Int, _ column: Int) {
        switch char {
        case "0": insert(0, 0, 0, 0, row, column)  // 0000
        case "1": insert(0, 0, 0, 1, row, column)  // 0001
        case "2": insert(0, 0, 1, 0, row, column)  // 0010
        case "3": insert(0, 0, 1, 1, row, column)  // 0011
        case "4": insert(0, 1, 0, 0, row, column)  // 0100
        case "5": insert(0, 1, 0, 1, row, column)  // 0101
        case "6": insert(0, 1, 1, 0, row, column)  // 0110
        case "7": insert(0, 1, 1, 1, row, column)  // 0111
        case "8": insert(1, 0, 0, 0, row, column)  // 1000
        case "9": insert(1, 0, 0, 1, row, column)  // 1001
        case "a": insert(1, 0, 1, 0, row, column)  // 1010
        case "b": insert(1, 0, 1, 1, row, column)  // 1011
        case "c": insert(1, 1, 0, 0, row, column)  // 1100
        case "d": insert(1, 1, 0, 1, row, column)  // 1101
        case "e": insert(1, 1, 1, 0, row, column)  // 1110
        case "f": insert(1, 1, 1, 1, row, column)  // 1111
        default: fatalError()
        }
    }

    func insert(_ b1: Int, _ b2: Int, _ b3: Int, _ b4: Int, _ r: Int, _ c: Int) {
        grid[Location(r, c + 0)] = b1 == 1
        grid[Location(r, c + 1)] = b2 == 1
        grid[Location(r, c + 2)] = b3 == 1
        grid[Location(r, c + 3)] = b4 == 1
    }
}
