//
//  Day19.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/22/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day19 {
    var grid = [Location: Character]()
    var location = Location.origin
    var direction = Direction.down

    func execute() {
        print("-- Day 19 --")
        let lines = Input.readLines(from: "day19", inDirectory: "2017")
        populateGrid(fromLines: lines)
        travel()
    }

    func populateGrid(fromLines lines: [String]) {
        let columnCount = lines[0].count

        for column in 0..<columnCount {
            let line = lines[0]
            let index = line.index(line.startIndex, offsetBy: column)

            grid[Location(column, 0)] = line[index]

            if line[index] == "|" {
                location.x = column
            }
        }

        for row in 1..<lines.count {
            let line = lines[row]
            for column in 0..<columnCount {
                let index = line.index(line.startIndex, offsetBy: column)
                grid[Location(column, row)] = line[index]
            }
        }
    }

    func travel() {
        var steps = 1
        var result: [Character] = []
        travelling: while true {
            location.move(inDirection: direction)

            if let char = grid[location] {
                switch char {
                case " ": break travelling
                case "+": changeDirection()
                case "|", "-":
                    // keep going
                    break
                default: result.append(char)
                }
            } else {
                break travelling
            }

            steps += 1
        }

        print("Letters = \(String(result))")
        print("Steps = \(steps)")
    }

    func changeDirection() {
        switch direction {
        case .up, .down: direction = grid[location.getAdjacent(.left)] != " " ? .left : .right
        case .left, .right: direction = grid[location.getAdjacent(.up)] != " " ? .up : .down
        }
    }
}
