//
//  Day05.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/5/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day05 {
    func execute() {
        print("-- Day 05 --")
        let lines = Input.readLines(from: "day05", inDirectory: "2017")
        var instructions = lines.map { Int($0) ?? 0 }

        var index = 0
        var steps = 0
        while index < instructions.count {
            let jump = instructions[index]
            instructions[index] = partTwo(offset: jump)
            index += jump
            steps += 1
        }

        print("Steps = \(steps)")
    }

    func partOne(offset: Int) -> Int {
        return offset + 1
    }

    func partTwo(offset: Int) -> Int {
        return offset > 2 ? offset - 1 : offset + 1
    }
}
