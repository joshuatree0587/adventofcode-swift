//
//  Day23.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 1/6/18.
//  Copyright © 2018 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day23 {
    func execute() {
        print("-- Day 23 --")
        let lines = Input.readLines(from: "day23", inDirectory: "2017")
        let instructions = lines.map { DuetInstruction(data: $0) }
        let program = DuetProgram(instructions, 0)
        while program.state == .success {
            program.update()
        }
        print("The mul instruction executed \(program.instructionCounts["mul"]!) times")
    }
}
