//
//  Day15.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/14/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day15 {
    // Generator A starts with 289
    let genA = Generator(seed: 289, factor: 16807)

    // Generator B starts with 629
    let genB = Generator(seed: 629, factor: 48271)

    let sixteenBits: UInt = 0b00000000_00000000_11111111_11111111

    func execute() {
        print("-- Day 15 --")
        generate(iterations: 40_000_000)

        genA.reset()
        genA.criteria = 4

        genB.reset()
        genB.criteria = 8

        generate(iterations: 5_000_000)
    }

    func generate(iterations: UInt) {
        var matches = 0
        for _ in 0..<iterations {
            let numA = genA.next()
            let numB = genB.next()
            if numA & sixteenBits == numB & sixteenBits {
                matches += 1
            }
        }
        print("\(matches) matches found")
    }

    class Generator {
        var seed: UInt
        var factor: UInt
        var criteria: UInt

        private var previous: UInt

        init(seed: UInt, factor: UInt) {
            self.seed = seed
            self.factor = factor
            self.criteria = 1
            self.previous = seed
        }

        func reset() {
            previous = seed
        }

        func next() -> UInt {
            repeat {
                previous = (previous * factor) % 2_147_483_647
            } while previous % criteria != 0
            return previous
        }
    }
}
