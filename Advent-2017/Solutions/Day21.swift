//
//  Day21.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/26/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

// TODO: Try storing patterns as 2-dimensional arrays instead
class Day21 {
    var grid: Grid = Grid(".#./..#/###")

    func execute() {
        print("-- Day 21 --")
        let lines = Input.readLines(from: "day21", inDirectory: "2017")
        let rules = lines.map { Rule($0) }
        grid.addRules(rules)

        print("0 iterations: There are \(grid.pattern.onPixelCount) pixels ON")

        for i in 1...18 {
            grid.enhance()
            print("\(i) iterations: There are \(grid.pattern.onPixelCount) pixels ON")
        }
    }

    class Grid {
        var rules: [String: Rule]
        var pattern: Pattern

        init(_ data: String) {
            pattern = Pattern(data)
            rules = [:]
        }

        func addRules(_ ruleList: [Rule]) {
            for rule in ruleList {
                let original = Pattern(rule.source)
                addRotatedRules(pattern: original, rule: rule)

                let flipHorizontal = original.flipHorizontal()
                addRotatedRules(pattern: flipHorizontal, rule: rule)

                // As it turns out, these would be redundant:
                //let flipVertical = original.flipVerticle()
                //addRotatedRules(pattern: flipVertical, rule: rule)
            }
        }

        func addRotatedRules(pattern: Pattern, rule: Rule) {
            rules[pattern.description] = rule
            var rotated = pattern.rotate()
            rules[rotated.description] = rule
            rotated = rotated.rotate()
            rules[rotated.description] = rule
            rotated = rotated.rotate()
            rules[rotated.description] = rule
        }

        func enhance() {
            var sections = [Location: Pattern]()
            var sectionSize = 0

            if pattern.size % 2 == 0 {
                sectionSize = 2
            } else if pattern.size % 3 == 0 {
                sectionSize = 3
            } else {
                fatalError()
            }

            for x in stride(from: 0, to: pattern.size, by: sectionSize) {
                for y in stride(from: 0, to: pattern.size, by: sectionSize) {
                    sections[Location(x / sectionSize, y / sectionSize)] =
                        pattern.extract(column: x, row: y, size: sectionSize)
                }
            }

            for entry in sections {
                let rule = rules[entry.value.description]!
                sections[entry.key] = rule.result
            }

            sectionSize += 1

            let gridSize: Int = Int(sqrt(Double(sections.count)))
            pattern = Pattern(sectionSize * gridSize)

            for column in 0..<gridSize {
                for row in 0..<gridSize {
                    let section = sections[Location(column, row)]!
                    pattern.insert(
                        pattern: section, column: column * sectionSize, row: row * sectionSize)
                }
            }
        }
    }

    class Rule {
        var source: String
        var result: Pattern

        init(_ data: String) {
            let components = data.components(separatedBy: " => ")
            source = components[0]
            result = Pattern(components[1])
        }
    }

    class Pattern: CustomStringConvertible {
        var size = 0
        private var pixels = [Location: Bool]()

        var description: String {
            var result = ""
            for row in 0..<size {
                for column in 0..<size {
                    result += pixels[Location(column, row)]! ? "#" : "."
                }
                if row < size - 1 {
                    result += "/"
                }
            }
            return result
        }

        var onPixelCount: Int {
            var result = 0
            for entry in pixels {
                if entry.value {
                    result += 1
                }
            }
            return result
        }

        init(_ data: String) {
            let components = data.components(separatedBy: "/")
            size = components.count

            for rowIndex in 0..<size {
                let rowData = components[rowIndex]
                for columnIndex in 0..<size {
                    let pixelData = rowData[
                        rowData.index(rowData.startIndex, offsetBy: columnIndex)]
                    pixels[Location(columnIndex, rowIndex)] = pixelData == "#"
                }
            }
        }

        init(_ size: Int) {
            self.size = size
        }

        func rotate() -> Pattern {
            let result = Pattern(size)
            for pixel in pixels {
                let column = (size - 1) - pixel.key.y
                let row = pixel.key.x
                result.pixels[Location(column, row)] = pixel.value
            }
            return result
        }

        func flipHorizontal() -> Pattern {
            let result = Pattern(size)
            for pixel in pixels {
                let column = (size - 1) - pixel.key.x
                let row = pixel.key.y
                result.pixels[Location(column, row)] = pixel.value
            }
            return result
        }

        func flipVerticle() -> Pattern {
            let result = Pattern(size)
            for pixel in pixels {
                let column = pixel.key.x
                let row = (size - 1) - pixel.key.y
                result.pixels[Location(column, row)] = pixel.value
            }
            return result
        }

        func extract(column: Int, row: Int, size: Int) -> Pattern {
            let result = Pattern(size)
            if size == 2 {
                result.pixels[Location(0, 0)] = self.pixels[Location(column + 0, row + 0)]
                result.pixels[Location(1, 0)] = self.pixels[Location(column + 1, row + 0)]
                result.pixels[Location(0, 1)] = self.pixels[Location(column + 0, row + 1)]
                result.pixels[Location(1, 1)] = self.pixels[Location(column + 1, row + 1)]
            } else if size == 3 {
                result.pixels[Location(0, 0)] = self.pixels[Location(column + 0, row + 0)]
                result.pixels[Location(1, 0)] = self.pixels[Location(column + 1, row + 0)]
                result.pixels[Location(2, 0)] = self.pixels[Location(column + 2, row + 0)]
                result.pixels[Location(0, 1)] = self.pixels[Location(column + 0, row + 1)]
                result.pixels[Location(1, 1)] = self.pixels[Location(column + 1, row + 1)]
                result.pixels[Location(2, 1)] = self.pixels[Location(column + 2, row + 1)]
                result.pixels[Location(0, 2)] = self.pixels[Location(column + 0, row + 2)]
                result.pixels[Location(1, 2)] = self.pixels[Location(column + 1, row + 2)]
                result.pixels[Location(2, 2)] = self.pixels[Location(column + 2, row + 2)]
            } else {
                fatalError()
            }
            return result
        }

        func insert(pattern target: Pattern, column offsetX: Int, row offsetY: Int) {
            for column in 0..<target.size {
                for row in 0..<target.size {
                    self.pixels[Location(offsetX + column, offsetY + row)] =
                        target.pixels[Location(column, row)]
                }
            }
        }
    }
}
