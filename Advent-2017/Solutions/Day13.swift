//
//  Day13.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/12/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day13 {
    var scanners = [Scanner]()

    func execute() {
        print("-- Day 13 --")
        //let input = "0: 3\n1: 2\n4: 4\n6: 4"
        let input = Input.readText(from: "day13", inDirectory: "2017")

        for entry in input.components(separatedBy: "\n") {
            let terms = entry.components(separatedBy: ": ")
            scanners.append(Scanner(withDepth: Int(terms[0])!, andRange: Int(terms[1])!))
        }

        scanners.sort(by: { $0.range < $1.range })

        partOne()
        partTwo()
    }

    func partOne() {
        var severity = 0
        for scanner in scanners {
            if scanner.position() == 0 {
                severity += scanner.depth * scanner.range
            }
        }
        print("The severity of the initial trip is \(severity)")
    }

    func partTwo() {
        var delay = 0
        while !test(withDelay: delay) {
            delay += 1
        }
        print("We need to delay for \(delay) picoseconds to avoid being caught")
    }

    func test(withDelay delay: Int) -> Bool {
        for scanner in scanners {
            if scanner.position(withDelay: delay) == 0 {
                return false
            }
        }
        return true
    }

    class Scanner {
        var depth = 0
        var range = 0

        init(withDepth depth: Int, andRange range: Int) {
            self.depth = depth
            self.range = range
        }

        func position(withDelay delay: Int = 0) -> Int {
            return (delay + depth) % ((range * 2) - 2)
        }
    }
}
