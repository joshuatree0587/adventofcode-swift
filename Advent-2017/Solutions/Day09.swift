//
//  Day09.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/9/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day09 {
    func execute() {
        print("-- Day 09 --")
        let input = Input.readText(from: "day09", inDirectory: "2017")

        var score = 0
        var characters = 0
        var depth = 0
        var garbage = false

        var iter = input.makeIterator()

        while let char = iter.next() {
            if garbage {
                switch char {
                case "!": _ = iter.next()
                case ">": garbage = false
                default: characters += 1
                }
            } else {
                switch char {
                case "<": garbage = true
                case "{": depth += 1
                case "}":
                    score += depth
                    depth -= 1
                default: break
                }
            }
        }

        print("The score is \(score)")
        print("There were \(characters) characters in the garbage")
    }
}
