//
//  Day07.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/7/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day07 {
    var programs = [String: Program]()

    func execute() {
        print("-- Day 07 --")
        let lines = Input.readLines(from: "day07", inDirectory: "2017")

        buildPrograms(entries: lines)

        linkPrograms()

        let root = findRootNode()

        print("The root program is \(root.name)")

        measure(program: root)

        //printTree(program: root, indent: "", last: true)

        _ = validate(program: root)
    }

    func buildPrograms(entries: [String]) {
        for entry in entries {
            let program = Program(data: entry)
            programs[program.name] = program
        }
    }

    func linkPrograms() {
        for program in programs.values {
            for name in program.childNames {
                let child = programs[name]!
                program.childPrograms.append(child)
                child.parentProgram = program
            }
        }
    }

    func findRootNode() -> Program {
        var root: Program?
        for program in programs.values {
            if program.parentProgram == nil {
                root = program
                break
            }
        }
        return root!
    }

    func printTree(program: Program, indent: String, last: Bool) {
        print("\(indent)+- \(program.name) (\(program.weight)) -> (\(program.totalWeight))")

        let nextIndent = indent + (last ? "   " : "|  ")

        for index in 0..<program.childPrograms.count {
            printTree(
                program: program.childPrograms[index],
                indent: nextIndent,
                last: index == program.childPrograms.count - 1)
        }
    }

    func measure(program: Program) {
        var total = program.weight

        for child in program.childPrograms {
            measure(program: child)
            total += child.totalWeight
        }

        program.totalWeight = total
    }

    func validate(program: Program) -> Bool {
        var weightmap = [Int: [Program]]()

        for child in program.childPrograms {
            if !validate(program: child) { return false }

            if weightmap[child.totalWeight] != nil {
                weightmap[child.totalWeight]!.append(child)
            } else {
                weightmap[child.totalWeight] = [child]
            }
        }

        if weightmap.count > 1 {
            print("Program \(program.name) has children of different weights!")
            for entry in weightmap {
                for child in entry.value {
                    print("-- \(child.name) (original = \(child.weight), total = \(entry.key))")
                }
            }
            return false
        }
        return true
    }

    class Program {
        static let junk = CharacterSet(charactersIn: ",()")

        // deserialized
        var name: String
        var weight: Int
        var childNames: [String]

        // computed
        var parentProgram: Program?
        var childPrograms: [Program]
        var totalWeight = 0

        init(data: String) {
            // Data will be in one of the following forms:
            // abcd (42)
            // xyzw (99) -> foo, bar, baz
            var components = data.components(separatedBy: " ")

            name = components.removeFirst()
            weight = Int(components.removeFirst().trimmingCharacters(in: Program.junk))!

            if components.count > 0 {
                components.removeFirst()  // removing "->"
                childNames = components.map { $0.trimmingCharacters(in: Program.junk) }
            } else {
                childNames = []
            }

            childPrograms = []
        }
    }
}
