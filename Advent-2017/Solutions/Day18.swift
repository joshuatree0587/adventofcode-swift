//
//  Day18.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/21/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day18 {
    func execute() {
        print("-- Day 18 --")
        let lines = Input.readLines(from: "day18", inDirectory: "2017")
        let instructions = lines.map { DuetInstruction(data: $0) }

        partOne(instructions)
        partTwo(instructions)
    }

    func partOne(_ instructions: [DuetInstruction]) {
        let program = DuetProgram(instructions, 0)
        while program.state == .success {
            program.update()
        }
    }

    func partTwo(_ instructions: [DuetInstruction]) {
        let alpha = DuetProgram(instructions, 0)
        let beta = DuetProgram(instructions, 1)
        alpha.partner = beta
        beta.partner = alpha

        while !((alpha.state == .wait && beta.state == .wait) ||
                (alpha.state == .exit && beta.state == .exit))
        {
            if alpha.state != .exit {
                alpha.update()
            }
            if beta.state != .exit {
                beta.update()
            }
        }

        print("Part Two = \(beta.sendCount)")
    }
}
