//
//  Day01.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/1/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day01 {
    func execute() {
        print("-- Day 01 --")
        let input = Input.readText(from: "day01", inDirectory: "2017")
        var result = 0

        for index in 0..<input.count {
            // Part One : let target = (index + 1) % input.count
            // Paty Two : let target = (index + (input.count / 2)) % input.count
            let target = (index + (input.count / 2)) % input.count

            let curr = input.index(input.startIndex, offsetBy: index)
            let next = input.index(input.startIndex, offsetBy: target)

            if input[curr] == input[next] {
                //print("\(index) == \(target) : \(input[curr]) == \(input[next])")
                if let int = Int(String(input[curr])) {
                    //print("\(result) + \(int) =  \(result + int)")
                    result += int
                }
            }
        }

        print("Solution = \(result)")
    }
}
