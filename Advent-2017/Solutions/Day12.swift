//
//  Day12.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/12/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day12 {
    var nodes = [Int: Node]()
    var group = Set<Int>()
    var visited = Set<Int>()
    var numGroups = 0

    func execute() {
        print("-- Day 12 --")
        let input = Input.readText(from: "day12", inDirectory: "2017")

        let junk = CharacterSet(charactersIn: ",")
        for line in input.components(separatedBy: "\n") {
            let terms = line.components(separatedBy: " ")
            let node = Node()
            for index in 2..<terms.count {
                node.neighbors.insert(Int(terms[index].trimmingCharacters(in: junk))!)
            }
            nodes[Int(terms[0])!] = node
        }

        var index = 0
        while index < nodes.keys.max()! {
            if !visited.contains(index) {
                traverse(id: index)
                numGroups += 1

                print("Visited \(group.count) programs starting from \(index)")

                visited = visited.union(group)
                group = Set<Int>()
            }

            index += 1
        }

        print("There were \(numGroups) groups")
    }

    func traverse(id: Int) {
        if !group.contains(id) {
            group.insert(id)
            for neighbor in nodes[id]!.neighbors {
                traverse(id: neighbor)
            }
        }
    }

    class Node {
        var neighbors = Set<Int>()
    }
}
