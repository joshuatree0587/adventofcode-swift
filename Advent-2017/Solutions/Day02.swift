//
//  Day02.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/3/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day02 {
    var checksum = 0

    func execute() {
        print("-- Day 02 --")
        let lines = Input.readLines(from: "day02", inDirectory: "2017")
        for row in lines {
            let entries = row.components(separatedBy: "\t")
            let numbers = entries.map { Int($0)! }
            partTwo(numbers: numbers)
        }
        print("Checksum = \(checksum)")
    }

    func partOne(numbers: [Int]) {
        let delta = numbers.max()! - numbers.min()!
        checksum += delta
    }

    func partTwo(numbers: [Int]) {
        let sorted = numbers.sorted()
        for big in sorted.reversed() {
            for small in sorted {
                if small == big { break }

                if big % small == 0 {
                    checksum += big / small
                    return
                }
            }
        }
    }
}
