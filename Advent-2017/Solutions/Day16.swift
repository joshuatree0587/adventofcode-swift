//
//  Day16.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/16/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day16 {
    var programs = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"]

    func execute() {
        print("-- Day 16 --")
        let input = Input.readText(from: "day16", inDirectory: "2017")
        let moves = input.components(separatedBy: ",")

        // HACK: I ran this with an infinate loop and discovered that a pattern repeats every 30 iterations
        let count = 1_000_000_000 % 30

        for iter in 0..<count {
            for move in moves {
                let type = move.first
                let details = move.dropFirst().components(separatedBy: "/")

                switch type! {
                case "s":
                    spin(amount: Int(details[0])!)
                    break
                case "x":
                    exchange(left: Int(details[0])!, right: Int(details[1])!)
                    break
                case "p":
                    partner(left: details[0], right: details[1])
                    break
                default: fatalError()
                }
            }

            if iter == 0 {
                print("Part One = \(programs.joined())")
            }
        }

        print("Part Two = \(programs.joined())")
    }

    func spin(amount: Int) {
        var offset = (programs.count - amount) % programs.count
        if offset < 0 {
            offset += programs.count
        }
        if offset != 0 {
            let left = programs[0..<offset]
            let right = programs[offset..<programs.count]
            programs = [String](right + left)
        }
    }

    func exchange(left: Int, right: Int) {
        let temp = programs[left]
        programs[left] = programs[right]
        programs[right] = temp
    }

    func partner(left: String, right: String) {
        let indexLeft = programs.firstIndex(of: left)!
        let indexRight = programs.firstIndex(of: right)!
        exchange(left: indexLeft, right: indexRight)
    }
}
