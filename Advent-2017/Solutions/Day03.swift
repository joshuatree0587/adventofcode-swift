//
//  Day03.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/4/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day03 {
    let input = 347991

    var currentDir = Direction.right
    var currentLoc = Location.origin
    var currentNum = 1
    var bounds = Rectangle()

    func execute() {
        print("-- Day 03 --")
        //partOne()
        partTwo()
    }

    func partOne() {
        while currentNum < input {
            moveNext()
        }
        print("Distance to input = \(currentLoc.manhattanDistance(to: Location.origin))")
    }

    func partTwo() {
        var storage = [Location: Int]()
        storage[Location.origin] = 1

        var result = 0

        while result == 0 {
            moveNext()

            var total = 0
            for neighbor in currentLoc.getSurrounding() {
                if let value = storage[neighbor] { total += value }
            }

            storage[currentLoc] = total

            if total > input { result = total }
        }

        print("First value larger than input = \(result)")
    }

    func moveNext() {
        currentNum += 1

        switch currentDir {
        case .right:
            currentLoc.x += 1
            if currentLoc.x > bounds.right {
                bounds.right = currentLoc.x
                currentDir = currentDir.turnLeft()
            }
        case .up:
            currentLoc.y -= 1
            if currentLoc.y < bounds.top {
                bounds.top = currentLoc.y
                currentDir = currentDir.turnLeft()
            }
        case .left:
            currentLoc.x -= 1
            if currentLoc.x < bounds.left {
                bounds.left = currentLoc.x
                currentDir = currentDir.turnLeft()
            }
        case .down:
            currentLoc.y += 1
            if currentLoc.y > bounds.bottom {
                bounds.bottom = currentLoc.y
                currentDir = currentDir.turnLeft()
            }
        }
    }
}
