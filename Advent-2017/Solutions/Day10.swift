//
//  Day10.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/10/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day10 {
    let input = "63,144,180,149,1,255,167,84,125,65,188,0,2,254,229,24"

    func execute() {
        print("-- Day 10 --")
        partOne()
        partTwo()
    }

    func partOne() {
        let lengths = input.components(separatedBy: ",").map { Int($0)! }
        let hash = KnotHash.hash(lengths: lengths)
        print("Part One = \(hash[0] * hash[1])")
    }

    func partTwo() {
        let hash = KnotHash.hash(text: input)
        print("Part Two = \(hash)")
    }
}
