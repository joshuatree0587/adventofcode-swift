//
//  Day20.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/23/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day20 {
    func execute() {
        print("-- Day 20 --")
        let lines = Input.readLines(from: "day20", inDirectory: "2017")
        let particles = lines.map { Particle($0) }

        var aliveCount = particles.count
        var closest = 0

        var lastUpdate = CFAbsoluteTimeGetCurrent()

        while CFAbsoluteTimeGetCurrent() - lastUpdate < 5 {
            let prevAliveCount = aliveCount
            let prevClosest = closest
            var positions = [Vector3: [Int]]()

            for iter in 0..<particles.count {
                let particle = particles[iter]
                particle.update()

                if particle.distance < particles[closest].distance {
                    closest = iter
                }

                if particle.alive {
                    var list = positions[particle.position]
                    if list != nil {
                        list!.append(iter)
                        positions[particle.position] = list
                    } else {
                        positions[particle.position] = [iter]
                    }
                }
            }

            for position in positions {
                if position.value.count > 1 {
                    for index in position.value {
                        print("Killing particle \(index)")
                        particles[index].alive = false
                        aliveCount -= 1
                    }
                }
            }

            if closest != prevClosest {
                print("Particle \(closest) is now the closest")
                lastUpdate = CFAbsoluteTimeGetCurrent()
            }

            if aliveCount != prevAliveCount {
                print("Only \(aliveCount) particles remain")
            }
        }
    }

    class Particle {
        static let junk = CharacterSet(charactersIn: "pva=<>,")

        var position: Vector3
        var velocity: Vector3
        var acceleration: Vector3
        var alive: Bool

        init(_ data: String) {
            let components = data.components(separatedBy: " ")
            position = Vector3(components[0].trimmingCharacters(in: Particle.junk))
            velocity = Vector3(components[1].trimmingCharacters(in: Particle.junk))
            acceleration = Vector3(components[2].trimmingCharacters(in: Particle.junk))
            alive = true
        }

        func update() {
            velocity.add(acceleration)
            position.add(velocity)
        }

        var distance: Int {
            return abs(position.x) + abs(position.y) + abs(position.z)
        }
    }

    class Vector3: Equatable, Hashable {
        var x = 0
        var y = 0
        var z = 0

        func hash(into hasher: inout Hasher) {
            hasher.combine(x)
            hasher.combine(y)
            hasher.combine(z)
        }

        init(_ data: String) {
            let components = data.components(separatedBy: ",")
            x = Int(components[0])!
            y = Int(components[1])!
            z = Int(components[2])!
        }

        func add(_ other: Vector3) {
            x += other.x
            y += other.y
            z += other.z
        }

        static func == (left: Vector3, right: Vector3) -> Bool {
            return left.x == right.x && left.y == right.y && left.z == right.z
        }
    }
}
