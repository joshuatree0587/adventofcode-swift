//
//  Day22.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/30/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day22 {
    var direction = Direction.up
    var location = Location.origin
    var infections = [Location: NodeState]()
    var numBurstsCausingInfections = 0

    enum NodeState {
        case clean
        case weakened
        case infected
        case flagged
    }

    func execute() {
        print("-- Day 22 --")
        let lines = Input.readLines(from: "day22", inDirectory: "2017")
        createGrid(fromLines: lines)
        partTwo()
        print("\(numBurstsCausingInfections) bursts caused infections")
    }

    func createGrid(fromLines lines: [String]) {
        let half = (lines.count - 1) / 2
        for y in 0..<lines.count {
            let line = lines[y]
            for x in 0..<line.count {
                let char = line[line.index(line.startIndex, offsetBy: x)]
                infections[Location(x - half, y - half)] = char == "#" ? .infected : .clean
            }
        }
    }

    func partOne() {
        for _ in 0..<10000 {
            if infections[location] == NodeState.infected {
                direction = direction.turnRight()
                infections[location] = .clean
            } else {
                direction = direction.turnLeft()
                infections[location] = .infected
                numBurstsCausingInfections += 1
            }
            location.move(inDirection: direction)
        }
    }

    func partTwo() {
        for _ in 0..<10_000_000 {
            let state = infections[location] ?? .clean
            switch state {
            case .clean:
                direction = direction.turnLeft()
                infections[location] = .weakened
                break
            case .weakened:
                infections[location] = .infected
                numBurstsCausingInfections += 1
                break
            case .infected:
                direction = direction.turnRight()
                infections[location] = .flagged
                break
            case .flagged:
                direction = direction.turnAround()
                infections[location] = .clean
                break
            }
            location.move(inDirection: direction)
        }
    }
}
