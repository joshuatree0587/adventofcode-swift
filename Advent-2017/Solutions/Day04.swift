//
//  Day04.swift
//  Advent-2017
//
//  Created by Joshua Stachowski on 12/4/17.
//  Copyright © 2017 Joshua Stachowski. All rights reserved.
//

import Foundation

class Day04 {
    func execute() {
        print("-- Day 04 --")
        let lines = Input.readLines(from: "day04", inDirectory: "2017")
        var invalidCount = 0

        for phrase in lines {
            let words = partTwo(words: phrase.components(separatedBy: " "))
            var previous = ""
            for word in words {
                if word == previous {
                    invalidCount += 1
                    break
                }
                previous = word
            }
        }

        print("Invalid Passphrases = \(invalidCount)")
        print("Valid Passphrases = \(lines.count - invalidCount)")
    }

    func partOne(words: [String]) -> [String] {
        return words.sorted()
    }

    func partTwo(words: [String]) -> [String] {
        var result = [String]()
        for index in 0..<words.count {
            result.append(String(words[index].sorted()))
        }
        return result.sorted()
    }
}
